{-|
Module:         Adm.Sturec.SMS
Description:    computations to cajole SMS, the Student Mark System
Copyright:      Jashank Jeremy, 2018
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

`Adm.Sturec.SMS` is an abstraction over SMS.
It provides student record management.

-}
------------------------------------------------------------------------


module Adm.Sturec.SMS
  ( )
where

import           Adm.Types

-- sms smsaddfield smscalc smsfield smssync smsupdate
-- name postmark logsub

data Field a
data FieldType
  = Name
  | String
  | PatString String
    -- ^ A string that matches a pattern.
  | Set [(String, Char)]
    -- ^ A set of options; typically used for a @SpecialFlags@ field.
  | Mark
  | Text
  | Enum
  | Grade
  | Int
  | Num

data Record a = Map (UPI, Field a) a
