{-|
Module:         Adm.Autotest.Run.Interpreted
Description:    automated tests run via an interpreter.
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

-}
------------------------------------------------------------------------

module Adm.Autotest.Run.Interpreted
  ( Interpreted(..) )
where

import           Adm.Autotest (RunWith)
import qualified Adm.Autotest as Autotest

------------------------------------------------------------------------

data Interpreted =
  Interpreted
  { interpreter :: FilePath
    -- ^ The interpreter to use.
  , exec_file   :: FilePath
    -- ^ The file to execute, probably the student submission.
  }
instance RunWith Interpreted where
--  do_run :: Interpreted -> Activity -> Autotest -> IO TestResult
  do_run _ _ _ _ = undefined
