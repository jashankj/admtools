{-|
Module:         Adm.Autotest.Run.PlainExecutable
Description:    automated tests run as executables
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

-}
------------------------------------------------------------------------

module Adm.Autotest.Run.PlainExecutable
  ( PlainExecutable(..) )
where

import           Control.Monad
import qualified Data.ByteString as B
import           Data.Maybe
import qualified Data.Text as T
import           System.Exit
import           System.FilePath
import qualified System.IO as IO
import           System.Process
import           System.Timeout

import           Adm.Autotest ( RunWith, TestResult(..)
                              , argv, f_stdin, time_limit )
import qualified Adm.Autotest as Autotest
import           Adm.Utils.Proc

------------------------------------------------------------------------

data PlainExecutable = PlainExecutable { exec_file :: String }
instance RunWith PlainExecutable where
--  do_run :: PlainExecutable -> Activity -> Autotest -> IO TestResult
  do_run pe tmpdir act test = do
    let test_args'' = argv test
        test_args'  = fromMaybe [] test_args''
        test_args   = map T.unpack $ test_args'

    let test_proc =
          proc_default
          { cmdspec = RawCommand ("." </> (exec_file pe)) test_args
          , cwd = Just tmpdir
          , std_in = CreatePipe
          , std_out = CreatePipe
          , std_err = CreatePipe
          }

    -- And send it away to run.
    (Just test_in_h, Just test_out_h, Just test_err_h, test_handle) <-
      createProcess_ "autotest/do_run" test_proc

    -- Give it some stdin, and wrap up.
    when (isJust $ f_stdin test)
      (do text <- Autotest.load_bytes act $ f_stdin test
          B.hPut test_in_h text)
    IO.hClose test_in_h

    -- Wait for the process to come home.  If we don't get a result
    -- fast enough, shoot it down -- it gets a SIGTERM to the head,
    -- and we get an "exit" status.
    --
    -- (I'm actually quite impressed that `timeout` works here...  it
    -- seems like this is actually quite difficult to get right.)
    let timelim' = fromMaybe default_time_limit $ time_limit test
    let timelim = 1000 * 1000 * timelim'
    test_exit' <- timeout timelim $ waitForProcess test_handle
    test_exit <-
      if not . isNothing $ test_exit'
      then return $ fromJust test_exit'
      else do
          terminateProcess test_handle
          return $ ExitFailure (-1)

    -- Slurp stdout and stderr.
    test_out <- B.hGetContents test_out_h
    test_err <- B.hGetContents test_err_h
    IO.hClose test_out_h
    IO.hClose test_err_h

    return $ TestResult test_out test_err test_exit


------------------------------------------------------------------------

-- | The default subprocess time limit in seconds.
default_time_limit :: Int
default_time_limit = 3

------------------------------------------------------------------------
