{-|
Module:         Adm.Autotest.Report
Description:    report on a set of automated tests
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

-}
------------------------------------------------------------------------


module Adm.Autotest.Report
  ( tests
  , fallout
  , one_test
  )
where

import           Control.Monad
-- import qualified Data.Algorithm.Diff as DA
import qualified Data.Algorithm.Diff.Gestalt as DA
import           Data.Algorithm.DiffOutput (ppDiff)
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as Bc8
import           Data.Maybe
import           Data.String (IsString)
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import           System.Exit
import           System.IO as IO

import           Adm.Activity (Activity)
import           Adm.Autotest ( Autotest
                              , descr, f_stdin
                              , Options
                              , TestRunStatus(..)
                              , TestStatus(..)
                              , compiled, executed
                              , actual, expected
                              , CompileStatus(..)
                              , TestResult
                              , test_stdout, test_stderr, test_exit_val )
import qualified Adm.Autotest as Autotest
import           Adm.Utils.Color
import           Adm.Utils.Errx


------------------------------------------------------------------------

-- | Report on tests that ran.
tests :: Options -> [TestStatus] -> IO ()
tests opts ts = do
  when (n_tests == 0)
       (putStrLn . color_array_to_s $ notest_msg)
  putStrLn . color_array_to_s $ msg
  when ((n_tests > 0) && (n_tests == tests_passed))
       (putStrLn . color_array_to_s $ atp_msg)
  where
    msg =
      [ sgr [sgr_bold]
      , Just (show n_tests), Just " test(s):\t"
      , sgr [sgr_unbold, sgr_green]
      , Just (show tests_passed), Just " passed; "
      , sgr [sgr_red]
      , Just (show tests_failed), Just " failed; "
      , sgr [sgr_magenta]
      , Just (show tests_broken), Just " could not be run."
      , sgr [sgr_reset]
      ]
    atp_msg =
      [ sgr [sgr_green, sgr_bold]
      , Just "All tests passed.  You are awesome!"
      , sgr [sgr_reset]
      ]
    notest_msg =
      [ sgr [sgr_bold]
      , Just "There aren't any Autotests for this activity!\n"
      , sgr [sgr_reset]
      ]
    (n_tests, tests_passed, tests_failed, tests_broken) =
      Autotest.test_statistics ts
    sgr = do_sgr $ Autotest.colorize opts


------------------------------------------------------------------------

one_test :: Int -> Autotest -> TestStatus -> IO ()
one_test n test ts =
  let test_name = T.unpack $ descr test
  in IO.hPutStrLn IO.stdout $
     ( "test " ++ (show n) ++ " (" ++ test_name ++ "): "
    ++ (show $ executed ts)
     )


------------------------------------------------------------------------

test_input_msg      :: IsString a => a
test_actual_msg     :: IsString a => a -> [a]
test_expected_msg   :: IsString a => a -> [a]
test_exit_value_msg :: IsString a => a -> a -> [a]
test_diff_msg       :: ([SGR] -> Maybe String) -> [Maybe String]
test_command_msg    :: IsString a => a
test_no_output_msg  :: IsString a => a
test_no_newline_msg :: IsString a => a

test_input_msg =
  "The following input was provided to your program:"
test_actual_msg x =
  [ "The following is the actual ", x, " produced:" ]
test_expected_msg x =
  [ "The following is the expected ", x, ":" ]
test_exit_value_msg x y =
  [ "The program exited ", x, ".\n"
  , "We expected it to exit ", y, "."
  ]
test_diff_msg sgr =
  [ Just "The following is a listing of differences "
  , Just "between your output (in "
  ] ++ color sgr_red "red" ++
  [ Just ",\n preceded by '"
  ] ++ color sgr_red "<" ++
  [ Just "'), and the expected output (in "
  ] ++ color sgr_green "green" ++
  [ Just ", preceded by '"
  ] ++ color sgr_green ">" ++
  [ Just "')\nfor this test:" ]
  where color c s = [ sgr [ c ], Just s, sgr [ sgr_reset, sgr_bold ] ]
test_command_msg =
  "The following command will reproduce this test:"
test_no_output_msg =
  "\\ No output."
test_no_newline_msg =
  "\\ No newline at end of output."


-- | Optional post-test fallout reporting.
fallout :: Options -> Activity -> Autotest -> TestStatus -> IO ()
fallout opts act test ts =
  when (executed ts /= TestPassed &&
        compiled ts == CompileOk)
       (fallout' opts act test ts)

-- | In the event that a test went wrong, tell the users why.
fallout' :: Options -> Activity -> Autotest -> TestStatus -> IO ()
fallout' opts act test ts = do
  when (Autotest.show_input opts) show_input

  when (differs (test_stdout) ts) show_stdout
  when (differs (test_stderr) ts) show_stderr
  when (differs (test_exit_val) ts) show_exit_value

  -- XXX report command to reproduce test
  -- do_maybe_show_reproduce

  where
    show_input :: IO ()
    show_input =
      when (isJust $ f_stdin test)
        (do text <- Autotest.load_bytes act $ f_stdin test
            test_report test_input_msg $ Just text)

    show_stdout :: IO ()
    show_stdout = do
      when (Autotest.show_expected opts) stdout_expected
      when (Autotest.show_actual opts)   stdout_actual
      when (Autotest.show_diff opts)     stdout_diff
      where
        stdout_expected
          = test_report_any_expected
            (concat $ test_expected_msg "output")
            (test_stdout)
        stdout_actual
          = test_report_any_actual
            (concat $ test_actual_msg "output")
            (test_stdout)
        stdout_diff
          = test_report_any_diff (test_stdout)

    show_stderr :: IO ()
    show_stderr = do
      when (Autotest.show_expected opts) stderr_expected
      when (Autotest.show_actual opts)   stderr_actual
      when (Autotest.show_diff opts)     stderr_diff
      where
        stderr_expected
          = test_report_any_expected
            (concat $ test_expected_msg "output")
            (test_stderr)
        stderr_actual
          = test_report_any_actual
            (concat $ test_actual_msg "output")
            (test_stderr)
        stderr_diff
          = test_report_any_diff (test_stderr)

    show_exit_value :: IO ()
    show_exit_value =
      test_report_header $ concat $
        test_exit_value_msg
        (show_reasonably akt_) (show_reasonably exp_)
      where
        (Just exp_) = fmap (test_exit_val) $ expected ts
        (Just akt_) = fmap (test_exit_val) $ actual   ts
        show_reasonably x =
          case x of
            ExitFailure (-1) -> "forcibly (time limit exceeded)"
            _ -> concat ["with exit status ", show_reasonably' x]
        show_reasonably' x =
          let (s, n) = case x of
                ExitSuccess      -> (retval_to_ex 0, 0)
                ExitFailure t    -> (retval_to_ex t, t)
          in case s of
               Just s' -> ((show s') ++ " (" ++ (show n) ++ ")")
               Nothing -> show n

    differs :: Eq a => (TestResult -> a) -> TestStatus -> Bool
    differs f ts_ =
      case (expected ts_, actual ts_) of
        (Just exp', Just akt') -> (f exp') /= (f akt')
        (Nothing,   Nothing)   -> False
        _                      -> True

    test_report_any_expected
      :: String
      -> (TestResult -> Bc8.ByteString)
      -> IO ()
    test_report_any_expected msg obs
      = test_report msg $
        fmap (obs) $ expected ts

    test_report_any_actual
      :: String
      -> (TestResult -> Bc8.ByteString)
      -> IO ()
    test_report_any_actual msg obs
      = test_report msg $
        fmap (obs) $ actual ts

    test_report_any_diff
      :: (TestResult -> Bc8.ByteString)
      -> IO ()
    test_report_any_diff obs =
      test_report (color_array_to_s $ test_diff_msg sgr) $
        Just . TE.encodeUtf8 $
          test_do_diff obs

    -- test_do_diff
    --   :: Options
    --   -> TestStatus
    --   -> ((Bc8.ByteString -> Const Bc8.ByteString Bc8.ByteString)
    --       -> TestResult -> Const Bc8.ByteString TestResult)
    --   -> Text
    test_do_diff
      :: (TestResult -> Bc8.ByteString)
      -> Text
    test_do_diff obs =
      T.pack . maybe_diff_colorize . ppDiff $ diff
      where
        diff = DA.diff act_ exp_
          -- getDiff [act_] [exp_]
        exp_ = to_s $ hoist $ expected ts
        act_ = to_s $ hoist $ actual   ts
        hoist = fromMaybe Bc8.empty . fmap (obs)
        maybe_diff_colorize
          | Autotest.colorize opts = unlines . diff_color . lines
          | otherwise              = id
        to_s = lines . T.unpack . TE.decodeUtf8


    test_report :: String -> Maybe Bc8.ByteString -> IO ()
    test_report msg bytes' = do
      let bytes = fromMaybe Bc8.empty bytes'
      test_report_header msg
      B.hPut IO.stdout bytes
      when (B.null bytes)
        test_report_no_output
      when ((not . B.null $ bytes) && (Bc8.last bytes) /= '\n')
        test_report_no_newline
      puts [Just "\n"]
      where
        test_report_no_output
          = test_report_interesting $ [test_no_output_msg]
        test_report_no_newline
          = test_report_interesting $ ["\n" ++ test_no_newline_msg]
        test_report_interesting facts
          = puts $
              [ sgr [ sgr_grey ] ]
              ++ map (Just) facts ++
              [ sgr [ sgr_reset ] ]

    test_report_header :: String -> IO ()
    test_report_header msg
      = puts $
          [ Just "\n"
          , sgr [ sgr_bold ]
          , Just msg
          , sgr [ sgr_reset ]
          , Just "\n\n"
          ]

    puts :: [Maybe String] -> IO ()
    puts = (IO.hPutStr IO.stdout) . color_array_to_s

    sgr = do_sgr $ Autotest.colorize opts

    {-
-- Needs lenses.
fallout' opts_ act_ test_ ts_ = do
  when (Autotest.show_input opts_)
       (test_report_input opts_ act_ test_ ts_)

  when (should_show ts_ test_stdout)
    (do when (Autotest.show_expected opts_)
          (test_report_stdout_expected opts_ act_ test_ ts_)
        when (Autotest.show_actual opts_)
          (test_report_stdout_actual opts_ act_ test_ ts_)
        when (Autotest.show_diff opts_)
          (test_report_stdout_diff opts_ act_ test_ ts_))

  when (should_show ts_ test_stderr)
    (do when (Autotest.show_expected opts_)
          (test_report_stderr_expected opts_ act_ test_ ts_)
        when (Autotest.show_actual opts_)
          (test_report_stderr_actual opts_ act_ test_ ts_)
        when (Autotest.show_diff opts_)
          (test_report_stderr_diff opts_ act_ test_ ts_))

  when ((should_show_exit_val ts_) &&
        (Autotest.show_exit_value opts_))
    (test_report_exit_value opts_ act_ test_ ts_)

  {-- XXX report command to reproduce test
  when (opts_ ^. show_command)
       (test_report_command opts_ act_ test_ ts_)
  --}

  where
    test_input_msg =
      "The following input was provided to your program:"
    test_report_input _opts act test _ts = do
      when (isJust $ f_stdin test)
        (do text <- Autotest.load_bytes act $ f_stdin test
            test_report test_input_msg text)

    should_show ts test_x =
      let (Just exp) = test_x $ expected ts
          (Just akt) = test_x $ actual   ts
      in exp /= akt
    should_show_exit_val ts =
      let (Just exp) = fmap (test_exit_val) $ expected ts
          (Just akt) = fmap (test_exit_val) $ actual ts
      in exp /= akt

    test_actual_msg x =
      "The following is the actual " ++ x ++ " produced:"
    test_report_any_actual msg obs _opts _act _test ts = do
      test_report msg $
        fmap (obs) actual ts
        -- ts ^. actual . _Just . obs

    test_expected_msg x =
      "The following is the expected " ++ x ++ ":"
    test_report_any_expected msg obs _opts _act _test ts = do
      test_report msg $
        fmap (obs) expected ts

    test_diff_msg = color_array_to_s
      [ Just "The following is "
      , Just "a listing of differences "
      , Just "between your output "
      , Just "(in "
      , sgr [sgr_red]
      , Just "red"
      , sgr [sgr_reset, sgr_bold]
      , Just ",\npreceded by '"
      , sgr [sgr_red]
      , Just "<"
      , sgr [sgr_reset, sgr_bold]
      , Just "'), and the expected output "
      , Just "(in "
      , sgr [sgr_green]
      , Just "green"
      , sgr [sgr_reset, sgr_bold]
      , Just ", preceded by '"
      , sgr [sgr_green]
      , Just ">"
      , sgr [sgr_reset, sgr_bold]
      , Just "')\nfor this test:"
      ]
    test_report_any_diff obs opts _act _test ts = do
      test_report test_diff_msg $
        TE.encodeUtf8 $ test_do_diff opts ts obs

    test_report_stdout_expected =
      test_report_any_expected
      (test_expected_msg "output")
      test_stdout
    test_report_stdout_actual =
      test_report_any_actual
      (test_actual_msg "output")
      test_stdout
    test_report_stdout_diff =
      test_report_any_diff test_stdout

    test_report_stderr_expected =
      test_report_any_expected
      (test_expected_msg "error(s)")
      test_stderr
    test_report_stderr_actual =
      test_report_any_actual
      (test_actual_msg "error(s)")
      test_stderr
    test_report_stderr_diff =
      test_report_any_diff test_stderr

    {--
    test_command_msg =
      "The following command will reproduce this test:"
    test_report_command _opts _act _test _ts = do
      test_report test_command_msg $
        TE.encodeUtf8 $
          "[test_command not yet implemented; yell at jashankj]"
    --}

    test_report_exit_value _opts _act _test ts = do
      test_report_header test_exit_value_msg
      where
        (Just expect'') = expected ts
        (Just actual'') = actual   ts
        show_reasonably x =
          case x of
            ExitFailure (-1) -> "forcibly (time limit exceeded)"
            _ -> concat ["with exit status ", show_reasonably' x]
        show_reasonably' x =
          let (s, n) = case x of
                ExitSuccess      -> (retval_to_ex $ 0, 0)
                ExitFailure t    -> (retval_to_ex t, t)
          in case s of
               Just s' -> ((show s') ++ " (" ++ (show n) ++ ")")
               Nothing -> show n
        test_exit_value_msg =
          concat
          [ "The program exited "
          , show_reasonably $ test_exit_val actual''
          , ".\nWe expected it to exit "
          , show_reasonably $ test_exit_val expect''
          , "."
          ]

    test_report_header msg = do
      IO.hPutStr IO.stdout $
        concat . catMaybes $
        [ Just "\n"
        , sgr [sgr_bold]
        , Just msg
        , sgr [sgr_reset]
        , Just "\n\n"
        ]

    test_report msg bytes = do
      test_report_header msg
      B.hPut IO.stdout bytes
      when (B.null $ bytes)
           (IO.hPutStrLn IO.stdout $
            color_array_to_s $
            [ sgr [sgr_grey]
            , Just "\\ No output."
            , sgr [sgr_reset]
            ])
      when ((not . B.null $ bytes) && (Bc8.last bytes) /= '\n')
           (IO.hPutStrLn IO.stdout $
            color_array_to_s $
            [ sgr [sgr_grey]
            , Just "\n\\ No newline at end of output."
            , sgr [sgr_reset]
            ])
      IO.hPutStr IO.stdout $ "\n"

    sgr = do_sgr $ Autotest.colorize opts_
-}

------------------------------------------------------------------------

