{-|
Module:         Adm.Autotest.Compile.CC
Description:    automated tests compiled with a C compiler
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

-}
------------------------------------------------------------------------

module Adm.Autotest.Compile.Cc
  ( Cc(..)
  )
where

import           Control.Monad
import           Data.Maybe
import qualified Data.Text as T
import           System.Exit
import qualified System.IO as IO
import           System.Process

import           Adm.Activity (activity, accept)
import           Adm.Autotest ( descr, compile_flags
                              , CompileWith(..)
                              , CompileStatus(..))
import           Adm.Utils.Proc

------------------------------------------------------------------------

data Cc = Cc
  { compiler :: String
  , extra_flags :: [String]
  }

instance Show Cc where
  show x =
    (compiler x) ++ (concat . extra_flags $ x)

instance CompileWith Cc where
  do_compile cc dir bin act test = do
    let act_ = T.unpack $ activity act
    let test_name' = descr test
        test_name  = T.unpack test_name'

    -- The list of sources is the list of files we accept, for now.
    -- XXX expand this when we start looking at function testing.
    let test_cflags'' = compile_flags test
    let test_cflags' = fromMaybe [] test_cflags''
    let test_cflags = map T.unpack $ test_cflags'
    let compile_sources = map T.unpack $ accept act
    let compile_args = ["-o", bin] ++ compile_sources ++ test_cflags


    -- OK, set up a compilation task.
    let cc_command =
          RawCommand (compiler cc) $ (extra_flags cc) ++ compile_args
    let cc_proc =
          proc_default
          { cmdspec = cc_command
          , cwd = Just dir
          , std_in = CreatePipe
          , std_out = CreatePipe
          , std_err = CreatePipe
          }

    -- And send it away to compile.
    (Just cc_in_h, Just cc_out_h, Just cc_err_h, cc_handle) <-
      createProcess_ "autotest/do_compile" cc_proc

    -- Close stdin, slurp stdout, slurp stderr, and wait for the
    -- process to come home.  Net result: this blocks on the compile.
    IO.hClose cc_in_h
    cc_out <- IO.hGetContents cc_out_h
    cc_err <- IO.hGetContents cc_err_h
    cc_exit <- waitForProcess cc_handle

    -- Did we compile successfully?  If not, give the user some
    -- friendly messages to tell them what went wrong.
    when (cc_exit /= ExitSuccess)
      (do
          IO.hPutStrLn IO.stderr $
            "autotest/do_compile: " ++
            "compilation errors occurred; " ++
            "aborting test '" ++ test_name ++ "' "++
            "for '" ++ act_ ++ "'."
          IO.hPutStrLn IO.stderr cc_out
          IO.hPutStrLn IO.stderr cc_err
      )

    -- Clean up; close handles only *after* we do the output calls.
    IO.hClose cc_out_h
    IO.hClose cc_err_h

    -- OK, how did we go?
    return $
      if cc_exit == ExitSuccess
      then CompileOk
      else CompileFailed


------------------------------------------------------------------------
