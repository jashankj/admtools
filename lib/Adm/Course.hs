{-|
Module:         Adm.Course
Description:    group information about a course
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

The `Adm.Course` library groups together
information about course staff and timetabling.
It uses @config.toml@ to do its job.

-}
------------------------------------------------------------------------


module Adm.Course
  ( load, load'
  , groups

  , Configuration(..)
  , Course(..)
  , Staff(..)
  , Class(..), ClassType(..), ClassMeeting(..)
  , Week(..)
  , SiteConfig(..)
  )
where

import           Data.Aeson
import           Data.Maybe
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import           Data.Time
import           GHC.Generics
import           System.FilePath
import           System.IO (IOMode(..), withFile)

import           Adm.Types
import qualified Adm.Utils.Paths as Paths
import qualified Adm.Utils.Toml as Toml

------------------------------------------------------------------------

-- | Course-wide configuration file.
data Configuration = Configuration
  { course :: Course
  , staff  :: [Staff]
  , class_ :: [Class]
  , week   :: [Week]
  , site   :: SiteConfig
  } deriving (Show, Eq, Generic)

-- | Course.
data Course = Course
  { course_code :: Text
  , course_account :: Text
  , course_session :: Text
  , course_email :: Text
  , course_name :: Text
  } deriving (Show, Eq, Generic)

-- | Each staff member.
data Staff = Staff
  { staff_upi :: UPI
  , staff_user :: [User]
  , staff_name :: Text
  , staff_role :: StaffRole
  } deriving (Show, Eq, Generic)
data StaffRole
  = Convenor
  | Lecturer
  | Tutor
  | Admin
  deriving (Show, Eq, Generic)

-- | Each class.
data Class = Class
  { class_type     :: ClassType
  , class_nss_id   :: Int
  , class_nss_code :: Text
  , class_handle   :: Text
  , class_meeting  :: [ClassMeeting]
  } deriving (Show, Eq, Generic)
data ClassType = Lecture | Tutorial | Laboratory | TutLabBlock | Seminar
  deriving (Show, Eq, Generic)
data ClassMeeting = ClassMeeting
  { meeting_location :: Text
  , meeting_day  :: Text
  , meeting_start :: Text
  , meeting_end   :: Text
  , meeting_weeks :: Text
  , meeting_instructor :: [UPI]
  } deriving (Show, Eq, Generic)

-- | Each week.
data Week = Week
  { week_ :: Int
  , week_release_activities :: UTCTime
  , week_release_solutions :: UTCTime
  } deriving (Show, Eq, Generic)

-- | Site-specific configuration.
data SiteConfig = SiteConfig
  { site_navbar :: [(FilePath, String)]
  } deriving (Show, Eq, Generic)

------------------------------------------------------------------------

instance FromJSON Configuration where
  parseJSON = withObject "Configuration" $
    \v -> Configuration
      <$> v .:  "course"
      <*> v .:  "staff"
      <*> v .:  "class"
      <*> v .:  "week"
      <*> v .:  "site"

instance FromJSON Course where
  parseJSON = withObject "Course" $
    \v -> Course
      <$> v .:  "code"
      <*> v .:  "account"
      <*> v .:  "session"
      <*> v .:  "email"
      <*> v .:  "name"

instance FromJSON StaffRole
instance FromJSON Staff where
  parseJSON = withObject "Staff" $
    \v -> Staff
      <$> v .:  "upi"
      <*> v .:  "user"
      <*> v .:  "name"
      <*> v .:  "role"

instance FromJSON Class where
  parseJSON = withObject "Class" $
    \v -> Class
      <$> v .:  "type"
      <*> v .:  "nss_id"
      <*> v .:  "nss_code"
      <*> v .:  "handle"
      <*> v .:  "meeting"
instance FromJSON ClassType where
  parseJSON = withText "ClassType" $
    \case
      "Lecture"    -> return Lecture
      "Lec"        -> return Lecture
      "Tutorial"   -> return Tutorial
      "Tut"        -> return Tutorial
      "Laboratory" -> return Laboratory
      "Lab"        -> return Laboratory
      "Seminar"    -> return Seminar
      "Sem"        -> return Seminar
      "TLB"        -> return TutLabBlock
      x            -> fail $ T.unpack $ T.concat ["unknown type '", x, "'"]
instance FromJSON ClassMeeting where
  parseJSON = withObject "ClassMeeting" $
    \v -> ClassMeeting
      <$> v .:  "location"
      <*> v .:  "day"
      <*> v .:  "start"
      <*> v .:  "end"
      <*> v .:  "weeks"
      <*> v .:  "instructor"

instance FromJSON Week where
  parseJSON = withObject "Week" $
    \v -> Week
      <$> v .:  "week"
      <*> v .:  "release_activities"
      <*> v .:  "release_solutions"

instance FromJSON SiteConfig where
  parseJSON = withObject "SiteConfig" $
    \v -> SiteConfig
      <$> v .:  "navbar"

------------------------------------------------------------------------

-- | Load in a course's configuration.
load :: IO Configuration
load = load' >>= return . fromJust

-- | Load in a course's configuration.
load' :: IO (Maybe Configuration)
load' =
  Paths.adm_data "config.toml"
  >>= \f -> withFile f ReadMode TIO.hGetContents
  >>= \s -> return $ Toml.maybe_parse f s

-- | Load the group list.
groups :: IO [Text]
groups = do
  out_ <- Paths.out'
  let groups_file = out_ </> "GROUPS"
  content <- TIO.readFile groups_file
  return $ T.lines content
