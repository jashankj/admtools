-- -*- haskell -*-
{-# OPTIONS_GHC -Wno-missing-methods #-}

module Adm.Autotest where

import Data.Aeson (FromJSON)
import GHC.Generics (Generic)

data Autotest
instance Show Autotest
instance Eq Autotest
instance Generic Autotest
instance FromJSON Autotest
