{-|
Module:         Adm.Give.Assign
Description:    create assignment specs from Activities
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

-}
------------------------------------------------------------------------


module Adm.Give.Assign
  ( make_assign_spec )
where

import           Data.Maybe
import qualified Data.Map as Map
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import           System.Directory

import           Adm.Activity ( Activity, ActivityType(..)
                              , act_type, activity, activity_type
                              , deadline, give_options, accept
                              )
import qualified Adm.Activity as Activity
import           Adm.Give
import qualified Adm.Give as G
import qualified Adm.Utils.Paths as Paths

-- | Take a spec for an assignment's give.spec, and turn it into a string
-- to be dumped into a file.
make_assign_spec :: Activity -> IO ()
make_assign_spec act = do
  let activity' = T.pack $ Activity.give_name act
      deadline' = deadline act
      opt_deadline = maybe [] (\x -> [Deadline [x]]) deadline'

      -- The spec proper.
      assign_spec :: [Directive 'AssignSpec]
      assign_spec =
          [ Assign activity'
          , Submission [subspec]
          , MaxSize 128
          , SMSMark activity'
          ] ++
            (get_latepen . give_options $ act) ++
            (get_extension . give_options $ act) ++
            (get_subkey act) ++
            opt_deadline ++
            (get_run_specs act)

  -- Create the varous directories we need to put the spec and give
  -- hierachy in.
  assign_give_dir <- Paths.assign_dir activity'
  createDirectoryIfMissing True assign_give_dir

  -- A friendly header comment to say where we hallucinated this file from.
  assign_give_spec <- Paths.assign_spec activity'
  TIO.writeFile assign_give_spec $
    from_assign_spec assign_spec
  where
    from_assign_spec spec =
      T.unlines $
      [ head_comment
      , autogen_comment
      , autogen_comment_
      , T.empty
      ] ++ (map (T.pack . show) spec)
    head_comment =
      G.comment . T.pack $ "COMP1511 17s2 -- The Giving Tree"
    autogen_comment =
      G.comment . T.pack $
      "This file was automatically generated from " ++
      "'" ++ (T.unpack activity'') ++ "/ACTIVITY.toml'."
    autogen_comment_ =
      G.comment . T.pack $
      "Check the activity source file, " ++
      "and run `mkgive` to regenerate."
    activity'' = activity act

    -- The submission spec.  For now, we have a dodgy hack.
    subspec = T.append "accept\t" $ T.unwords $ accept act

------------------------------------------------------------------------

get_subkey :: Activity -> [Directive 'AssignSpec]
get_subkey act =
  case activity act of
    "testGame" -> [SubKey "sms assign2_group"]
    "Game" -> [SubKey "sms assign2_group"]
    "player" -> [SubKey "sms lab_pairs_3"]
    _ -> get_subkey' act

-- | In week 4, we got group submissions going (at last!).
--
-- * week01 to week04:          pair names from `sms lab_pairs_1`
-- * week05 to week08; assign1: pair names from `sms lab_pairs_2`
-- * week09 to week13; assign2: pair names from `sms lab_pairs_3`
--
get_subkey' :: Activity -> [Directive 'AssignSpec]
get_subkey' act =
  case activity_type act of
    InWeek (4, _) ->
      if is_pair_activity
      then [SubKey "sms lab_pairs_1"] else []

    InWeek (w, _) | 5 <= w && w <= 8 ->
      if is_pair_activity
      then [SubKey "sms lab_pairs_2"] else []
    Assignment 1 ->
      [SubKey "sms lab_pairs_2"]

    InWeek (w, _) | 9 <= w && w <= 13 ->
      if is_pair_activity
      then [SubKey "sms lab_pairs_3"] else []
    Assignment 2 ->
      [SubKey "sms lab_pairs_3"]

    _ -> []
  where
    is_pair_activity =
      case act_type act of
        Just x  -> x == "pair"
        Nothing -> False

------------------------------------------------------------------------

get_extension :: Maybe GiveOptions -> [Directive 'AssignSpec]
get_extension Nothing = []
get_extension (Just opts) =
  let
    extensions = extension opts
    exts_list = Map.toList extensions
  in case length exts_list of
    0 -> []
    _ -> [Extension exts_list]


get_latepen :: Maybe GiveOptions -> [Directive 'AssignSpec]
get_latepen Nothing = []
get_latepen (Just opts) =
  case late_penalty opts of
    Just lp -> [LatePenalty $ lp]
    Nothing -> []


------------------------------------------------------------------------

get_run_specs :: Activity -> [Directive 'AssignSpec]
get_run_specs _act =
  get_run_specs' _act $ give_options _act
  where
    get_run_specs' :: Activity -> Maybe GiveOptions -> [Directive 'AssignSpec]
    get_run_specs' act Nothing     = default_run_spec act
    get_run_specs' act (Just opts) = get_run_specs'' act opts

    get_run_specs'' :: Activity -> GiveOptions -> [Directive 'AssignSpec]
    get_run_specs'' act opts =
      [ RunSpecs
      , TestMode NoTests
      , Compile $
        case give_compile opts of
          Just x  -> T.lines x
          Nothing -> do_listing act
      ] ++ (
      if isJust $ give_special opts
      then map (\x ->
          let mark = special_mark x
              descr_ = special_descr x
              script = T.lines $ special_script x
          in SpecialTest (mark, descr_) script
        ) . fromJust $ give_special opts
      else []
      ) ++ (
      case give_subjective opts of
        Just x  -> [ SubjectiveMark . Map.toList $ x ]
        Nothing -> []
      ) ++ (
      let subjective_ = give_subjective opts
          subjective  = fromMaybe Map.empty $ subjective_
          subjectiveMark = sum . Map.elems $ subjective
          specials_ = give_special opts
          specials  = fromMaybe [] $ specials_
          specialMark = sum . (map (special_mark)) $ specials
      in [TotalMark $ (subjectiveMark + specialMark)]
      )

-- | XXX make this more configurable
default_run_spec :: Activity -> [Directive 'AssignSpec]
default_run_spec act =
  [ RunSpecs
  , TestMode NoTests
  , Compile $ do_listing act
  ]

do_listing :: Activity -> [Text]
do_listing act =
  [ "rm -f *.c \\!dryrun_record"
  , "classify -v $1"
  , "cp -p $S/\\!dryrun_record ."
  , T.append "for f in " (T.unwords . accept $ act)
  , "do"
  , "    if [ -f $f ]"
  , "    then"
  , "        dos2unix -- $f"
  , "        list $f"
  , "    fi"
  , "done"
  , ""
  , T.concat
    [ "autotest --no-colorize --no-parallel "
    , (activity act)
    , " | stufilter +300 -20"
    ]
  ]


------------------------------------------------------------------------
