{-|
Module:         Adm.Give.Session
Description:    create session specs from a set of activities.
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

-}
------------------------------------------------------------------------


module Adm.Give.Session
  ( make_session_spec )
where

import           Data.List
-- import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

import           Adm.Activity (Activity)
import qualified Adm.Activity as Activity
import qualified Adm.Course as Course
import           Adm.Give
import qualified Adm.Give as G
import qualified Adm.Utils.Paths as Paths

-- | Take a spec for the session give.spec, and turn it into a string to
--   be dumped into a file.  This has the added advantage that we can
--   make sure the assignments, withholds, etc. are all in sync with the
--   tasks, and are in the right order and syntactically valid.
--
--   (Trivia time: there's a tool named GUMNUT that was intended to
--   build Give.spec files.  It still exists, but unfortunately, the
--   part that exists isn't _quite_ compatible with current Give spec
--   syntax... :-)
make_session_spec :: [Activity] -> IO ()
make_session_spec acts = do
  (_, class_user, _) <- Paths.give_env
  session_give_spec <- Paths.session_spec
  smsdb <- Paths.smsdb'

  -- Load information about the course.
  config <- Course.load
  let users = nub . sort
            $ concatMap (Course.staff_user)
            $ Course.staff config

  -- Currently, we enable MARK and RSMS for all, though that's
  -- non-optimal, and I suspect it'll be turned off in the near
  -- future.  For now, though, this will do:
  let can_mark' = True
      can_rsms' = True
      auth_spec u = AuthSpec u can_mark' can_rsms'
      authorisations = map auth_spec $ class_user : users

  -- The list of activities:
  let activities = map (T.pack . Activity.give_name) $ acts

  -- And now the session spec proper.
  let session_spec :: [Directive 'SessionSpec]
      session_spec =
        [ LoginMap []
        , Authorisation authorisations
        , SMSDB smsdb
        , SubOptions "z"
        , SubKey "stuID"
        , SubGroup "sms tut_lab"
        , Withhold []
        , AssignMap $ sort activities
        ]

  -- A friendly header comment, to remind people to check the
  -- activity source files... because we do.
  let comment_ = G.comment . T.pack
      head_comment =
        comment_ $
          "COMP1511 17s2 -- The Giving Tree"
      autogen_comment =
        comment_ $
          "This file is automatically regenerated.  " ++
          "You probably shouldn't edit it."
      autogen_comment_ =
        comment_ $
          "Check the activity source files, " ++
          "and run `mkgive` to regenerate."
      from_session_spec spec =
        T.unlines $
          [ head_comment
          , autogen_comment
          , autogen_comment_
          , T.empty
          ] ++ (map (T.pack . show) spec)

  -- And emit it.
  TIO.writeFile session_give_spec $
    from_session_spec session_spec

------------------------------------------------------------------------
