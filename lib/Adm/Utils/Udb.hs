{-|
Module:         Adm.Utils.Udb
Description:    An interface to the CSE user database.
Copyright:      Jashank Jeremy, 2018
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

An interface to UDB,
the user and group database at CSE,
via the @acc@ command.

-}


module Adm.Utils.Udb
  ( UdbUser(..)
  , acc, accSelf, doAcc, isOldCseUsername )
where

import           Data.Char
import           Data.List (intercalate, notElem, stripPrefix)
import           Data.List.Split (splitOn)
import           Data.Maybe (fromJust)
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Time
import           GHC.Generics
import           System.Process

import           Adm.Types

------------------------------------------------------------------------

-- | A user entry in the user database.
data UdbUser = UdbUser
  { user :: User -- ^ username
  , user_aliases :: [User] -- ^ other user aliases
  , user_shell :: FilePath -- ^ user's shell
  , user_upi :: UPI -- ^ user's UPI
  , user_uid :: Int -- ^ user's uid
  , user_gid :: Int -- ^ user's gid
  , user_home :: [FilePath] -- ^ User's home directory, primary first.
  , user_expiry :: Day -- ^ user account expiry date
  , user_title :: Text -- ^ user's title (e.g., @"Dr"@)
  , user_given :: Text -- ^ user's given name
  , user_last :: Text -- ^ user's surname
  , user_name :: Text -- ^ user's whole preferred name
  , user_role :: Text -- ^ user's role (e.g., @"Casual Academic"@)
  , user_school :: Text -- ^ user's affiliated school
  , user_faculty :: Text -- ^ user's affiliated faculty
  , user_unswmail :: Text -- ^ user's official UNSW email address
  , user_unswhome :: Text -- ^ user's unreliable home directory
  , user_groups :: [(String, Maybe Day)]
    -- ^ groups the user belongs to, and Just expiry dates or Nothing if
    -- the group affiliation never expires
  } deriving (Show, Eq, Generic)

class IsInUdb a where
  toUsername :: a -> User
instance IsInUdb User where
  toUsername = id
instance IsInUdb UPI where
  toUsername upi = T.pack $ "z" ++ (show upi)

------------------------------------------------------------------------

-- | Retrieve user information about the current user.
accSelf :: IO UdbUser
accSelf = acc (Nothing :: Maybe User)

-- | Retrieve user information from UDB.
acc :: IsInUdb u => Maybe u -> IO UdbUser
acc u_ = do
  -- retrieve information from acc.
  res <- doAcc $
    ("format=" ++ format) :
    case u_ of
      Just x  -> [T.unpack $ toUsername x]
      Nothing -> []
  let kvs = zip fields $ splitOn separator res
  let get k = fromJust $ lookup k kvs

  -- Do some light preprocessing and wrangling...
  let usernames
        = map (fromJust . stripPrefix "user.")
        $ splitOn "|" $ get "USER"

  -- ... and turn it into a UdbUser.
  return $ UdbUser
    { user          = T.pack $ head usernames
    , user_aliases  = map (T.pack) $ tail usernames
    , user_shell    = get "shell"
    , user_upi      = read $ get "upi"
    , user_uid      = read $ get "uid"
    , user_gid      = read $ get "gid"
    , user_home     = get "home"
                    : (splitOn " " $ get "otherhomes")
    , user_expiry   = accday $ get "expiry"
    , user_title    = T.pack $ get "title"
    , user_given    = T.pack $ get "given"
    , user_last     = T.pack $ get "last"
    , user_name     = T.pack $ get "name"
    , user_role     = T.pack $ get "role"
    , user_school   = T.pack $ get "school"
    , user_faculty  = T.pack $ get "faculty"
    , user_unswmail = T.pack $ get "unswmail"
    , user_unswhome = T.pack $ get "unswhome"
    , user_groups   =
      map (\gx ->
              let (g, t__) = break (== '[') gx
                  t_ = filter (\x -> x `notElem` ("[]" :: String)) t__
                  t = (parseTimeM False defaultTimeLocale "%s" t_ >>=
                       return . utctDay)
              in (g, t))
      $ splitOn ","
      $ get "E"
    }
  where
    format = intercalate separator
           $ map (\x -> concat ["${", x, "}"])
           $ fields
    separator = "###"
    fields = [ "USER", "shell", "upi", "uid", "gid"
             , "home", "otherhomes", "expiry"
             , "title", "given", "last", "name"
             , "role" , "school", "faculty"
             , "unswmail", "unswhome", "E" ]

    accday x_ =
      let x  = read x_
          yr =  x `div` 10000
          mo = (x `div`   100) `mod` 100
          da =  x              `mod` 100
      in fromGregorian (toInteger yr) mo da

-- | Retrieve information from the CSE user database using @acc@.
doAcc :: [String] -> IO String
doAcc args =
  readProcess "acc" args []

-- | Is this username an old-style CSE username (e.g., @cmil254@)?
isOldCseUsername :: String -> Bool
isOldCseUsername s =
  let
    (four, three') = splitAt 4 $ s
    three          = (take 3) $ three'
  in
    (all isAscii s) &&
    (all isAlpha four) &&
    (all isDigit three)
