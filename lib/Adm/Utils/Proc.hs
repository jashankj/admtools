{-|
Module:         Adm.Utils.Proc
Description:    Common process abstractions
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX
-}

module Adm.Utils.Proc
  ( proc_default )
where

import           System.Process (CreateProcess(..), proc)

------------------------------------------------------------------------

-- | The default set-up for a process.  We use this to minimise future
--   knob fiddling, and set some sane defaults:
--
--    * @cmdspec@ predictably fails by default, for safety.
--    * Inherit the current working directory and current environment.
--    * Standard input, output, and error are disconnected by default.
--    * Close all file descriptors on the new process except for the new
--      std{in,out,err}:
--    * Delegate C-c handling; users pressing @^C@ on the console will
--      interrupt a process under test.
--
--   To be portable, we start with `System.Process.proc`; it turns out
--   that the structure of the `CreateProcess` record changes in
--   non-trivial ways from version to version, and because the `process`
--   package is in the Platform, is bound to a specific GHC, more or
--   less.

proc_default :: CreateProcess
proc_default =
  sys_proc_proc
    { close_fds = True
    , delegate_ctlc = True
    }
  where
    sys_proc_proc = System.Process.proc "false" []

------------------------------------------------------------------------
