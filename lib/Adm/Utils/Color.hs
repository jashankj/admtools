{-|
Module:         Adm.Utils.Color
Description:    colourful text on a terminal!
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

Terminals have supported glorious colour for many years now.

Here's a simple set of functions that allow a relatively simple
interface to the ANSI escape sequences for colour.

-}


module Adm.Utils.Color
  ( color_array_to_s
  , do_sgr

    -- * Colorise output
  , diff_color
  , highlight_trailing_whitespace

    -- * Convenience exports
  , SGR
  , sgr_bold
  , sgr_unbold
  , sgr_red
  , sgr_grey
  , sgr_green
  , sgr_magenta
  , sgr_reset
  )
where

import           Data.Maybe
import           Data.Text (Text)
import qualified Data.Text as T
import           System.Console.ANSI

------------------------------------------------------------------------

-- Some shorthand hooks.

sgr_bold    :: SGR
sgr_bold     = SetConsoleIntensity BoldIntensity

sgr_unbold  :: SGR
sgr_unbold   = SetConsoleIntensity NormalIntensity

sgr_grey    :: SGR
sgr_grey     = SetConsoleIntensity FaintIntensity

sgr_green   :: SGR
sgr_green    = SetColor Foreground Vivid Green

sgr_red     :: SGR
sgr_red      = SetColor Foreground Vivid Red

sgr_magenta :: SGR
sgr_magenta  = SetColor Foreground Vivid Magenta

sgr_reset   :: SGR
sgr_reset    = Reset


------------------------------------------------------------------------

-- | Apply a colour.
--
--   >>> do_sgr False [sgr_bold]
--   Nothing
--
--   >>> do_sgr True [sgr_bold]
--   Just "\ESC[1m"
do_sgr
  :: Bool               -- ^ Given a Boolean specifying whether to
                        --   colourise output,
  -> [SGR]              -- ^ And given a list of SGRs,
  -> Maybe String       -- ^ Then, return a string of the escape
                        --   sequence necessary to produce that SGR,
                        --   or @Nothing@ if colours are disabled
do_sgr True x  = Just $ setSGRCode x
do_sgr False _ = Nothing

-- | An array of potentially coloured text.
--
-- >>> :{
-- bolded should_colour = color_array_to_s expr
--   where expr = [ sgr [sgr_bold]
--                , Just "Some bold text."
--                , sgr [sgr_reset]
--                ]
--         sgr = do_sgr should_colour
-- :}
--
-- >>> bolded False
-- "Some bold text."
-- >>> bolded True
-- "\ESC[1mSome bold text.\ESC[0m"
color_array_to_s :: [Maybe String] -> String
color_array_to_s = concat . catMaybes


------------------------------------------------------------------------

-- | Colourise a classic diff(1).
diff_color :: [String] -> [String]
diff_color str_ =
  map color_line $ str_
  where
    color_line [] = []
    color_line l =
      let l' = hl_trailing_space l
      in case head l of
           '>' -> concat [sgr [sgr_green], l', sgr [sgr_reset]]
           '<' -> concat [sgr [sgr_red],   l', sgr [sgr_reset]]
           '-' -> concat [sgr [sgr_grey],  l', sgr [sgr_reset]]
           _ -> l'
    hl_trailing_space =
      T.unpack . highlight_trailing_whitespace . T.pack
    sgr x = setSGRCode x


------------------------------------------------------------------------

-- | Highlight trailing whitespace in lurid red.
highlight_trailing_whitespace :: Text -> Text
highlight_trailing_whitespace = highlight . decompose
  where
    decompose t =
      let space_p = (== ' ')
          (not_space, space_) = tDropTakeEnd (space_p) t
          space = [ space_ | space_ /= T.empty ]
      in (not_space, space)
    highlight (x, Nothing) = x
    highlight (x, Just y)  =
      T.concat
        [ x
        , sgr [SetColor Background Vivid Red]
        , y
        , sgr [sgr_reset]
        ]
    sgr = T.pack . setSGRCode
    tDropTakeEnd p t = (T.dropWhileEnd (p) t, T.takeWhileEnd (p) t)
