{-|
Module:         Adm.Utils.Toml
Description:    sensibly parse TOML
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

`Adm.Utils.Toml` is a set of shims for parsing TOML using the `htoml`
library, which is apparently terrible unable to handle parse errors in
an idiomatic way.

-}
------------------------------------------------------------------------


module Adm.Utils.Toml
  ( parse
  , maybe_parse
  )
where

import           Data.Aeson  ( FromJSON, Result(Success, Error)
                             , toJSON, fromJSON )
import           Data.Either ( Either(Left, Right) )
import           Data.Maybe  ( fromMaybe )
import           Data.Text   ( Text )
import           Text.Toml   ( parseTomlDoc )

------------------------------------------------------------------------
-- $setup
-- >>> :set -XStandaloneDeriving
-- >>> :set -XDeriveGeneric
-- >>> :set -XOverloadedStrings
-- >>> import GHC.Generics
-- >>> import Data.Aeson
-- >>> import Text.Toml
-- >>> import Data.Text
-- >>> data X = X { a :: Int } deriving (Show, Eq, Generic)
-- >>> instance FromJSON X
-- >>> instance ToJSON X
-- >>> defaultX = X 0

-- | Parse a TOML document into some type @a@, which can be coerced
--   using Aeson.  See 'maybe_parse'.
--
--   >>> let x = parse "(nil)" "a = 4" defaultX in x :: X
--   X {a = 4}
--
--   >>> let x = parse "(nil)" "a = false" defaultX in x :: X
--   X {a = 0}
--
parse :: FromJSON a
      => FilePath       -- ^ And given the name of the file that
                        --   was loaded,
      -> Text           -- ^ And given the actual text that was
                        --   loaded from that file,
      -> a              -- ^ And given an `a` that gives a default
                        --   value in case of a parse failure,
      -> a              -- ^ Return the JSON-loaded `a` from the
                        --   string, or the passed otherwise value.
parse from_ str_ otherwise_ =
  fromMaybe otherwise_ $ maybe_parse from_ str_


-- | Parse a TOML document of type @a@, or return 'Nothing' on error.
--
--   >>> let x = maybe_parse "(nil)" "a = 4" in x :: Maybe X
--   Just (X {a = 4})
--
--   >>> let x = maybe_parse "(nil)" "a = false" in x :: Maybe X
--   Nothing
maybe_parse
   :: FromJSON a
   => FilePath          -- ^ And given the name of the file that
                        --   was loaded,
   -> Text              -- ^ And given the actual text that was
                        --   loaded from that file,
   -> Maybe a           -- ^ Maybe return the JSON-loadable `a`.
maybe_parse from str =
  pure (parseTomlDoc from str)
  >>= \case
        Right r -> return r
        Left l  -> fail $ "toml: " ++ show l
  >>= return . fromJSON . toJSON
  >>= \case
        Success r -> return r
        Error   l -> fail $ "json: " ++ show l

------------------------------------------------------------------------
