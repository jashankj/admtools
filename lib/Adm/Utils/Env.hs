{-|
Module:         Adm.Utils.Env
Description:    Clean up the environment
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

`Adm.Utils.Env` tidies up the program environment,
according to the environment spec passed in.

-}

module Adm.Utils.Env
  ( Env (..)
  , clean_up_environment
  )
where

import System.Environment

data Env = Env
  { safe_env_vars :: [String]
    -- ^ A list of environment variables that may be preserved;
    --   all others will be unset.
  , set_env_vars :: [(String, String)]
    -- ^ A list of (key, value) pairs to be set.
  }
  deriving (Show, Eq)

-- | Given a list of safe environment variables, and variable-value
--   pairs to set, fixes up the execution environment to match.
clean_up_environment :: Env -> IO ()
clean_up_environment (Env safe set)
  =  drop_bad_env_vars safe
  >> force_good_env_vars set
  where
    drop_bad_env_vars safe_env = do
      env_vars <- fmap (map fst) getEnvironment
      let safe_var = (`elem` safe_env)
      mapM_ (unsetEnv) $ filter (not . safe_var) env_vars
    force_good_env_vars good_env =
      mapM_ (\(k,v) -> setEnv k v) good_env
