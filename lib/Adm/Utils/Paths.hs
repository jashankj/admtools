{-|
Module:         Adm.Utils.Paths
Description:    Where to find various files
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

The `Adm.Utils.Paths` library specifies the locations where various
important files live or can be found.

=== amd, or How I Learned To Stop Worrying And Love The Automounter

On CSE, @\/home@, @\/web@, and @\/import@ are driven by the automounter,
so most interesting paths -- for example, @\/home\/cs1511\/give.spec@ --
require two realisation hops through @amd@, the context switch to which
is (allegedly) a performance bottleneck.

The obvious conclusion is to canonicalise up-front instead, except path
canonicalisation is also badly broken: @\/import\/HOST\/MOUNT@ paths are
really auto-generated symlinks to @\/tmp_amd\/HOST\/export\/HOST\/MOUNT@
(or, if we're on that host, @\/export\/HOST\/MOUNT@), but @\/tmp_amd@
paths /are not automounted/!  What?!

This led to all kinds of weird and wonderful problems in COMP1511 17s2
so... we wound up turning it off and backing out the code to do it.
The code is still there, behind the @SAFE_TO_CANONICALISE_PATHS@ macro.
Thanks for spotting that, Mei Cheng!

-}

{-# LANGUAGE CPP #-}

module Adm.Utils.Paths
  ( -- * The Give environment
    give_env

    -- * Course account conventions
  , user_to_class, class_to_user

    -- * Common Paths
  , home, home'
  , web, web'
  , session_web, session_web'
  , work, work'
  , smsdb, smsdb'
  , out, out'
  , marked, marked'
  , assign_dir, assign_dir'

    -- * Give specfile paths
  , global_spec, class_spec, session_spec, assign_spec

    -- * admtools paths
  , adm, adm'
  , adm_data
  , adm_activities
  )
where

import           Data.Map (Map)
import qualified Data.Map as Map
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Tuple (swap)
import           System.Environment (getEnv)
import           System.FilePath ( (</>), (<.>) )

#ifdef SAFE_TO_CANONICALISE_PATHS
import           System.Directory (canonicalizePath)
#endif

------------------------------------------------------------------------
-- $setup
-- >>> import System.Environment (setEnv)
-- >>> :{
-- mapM_ (\(k,v) -> setEnv k v)
--   [ ("CLASS",      "COMP1511")
--   , ("USER",       "cs1511")
--   , ("GIVEPERIOD", "17s2")
--   ]
-- :}

-- | `give_env` returns three important-ish Give environment variables:
--
--   * @$CLASS@, which contains the full course code,
--   * @$USER@, which is the course account name, and
--   * @$GIVEPERIOD@, which is the currently-active session.
--
--   >>> give_env
--   ("COMP1511","cs1511","17s2")
give_env :: IO (Text, Text, Text)
give_env = do
  class_   <- fmap T.pack give_class
  user_    <- fmap T.pack give_user
  session_ <- fmap T.pack give_period
  return (class_, user_, session_)

-- | Return the Give class, a course code.
--
--   >>> give_class
--   "COMP1511"
give_class :: IO String
give_class = getEnv "CLASS"

-- | Return the course account name.
--
--   >>> give_user
--   "cs1511"
give_user :: IO String
give_user = fmap class_to_user give_class

-- | Return the Give period.
--
--   >>> give_period
--   "17s2"
give_period :: IO String
give_period = getEnv "GIVEPERIOD"

------------------------------------------------------------------------

-- | Convert a username to a full course code.
--
--   >>> user_to_class "cs1511"
--   "COMP1511"
user_to_class
  :: String             -- ^ Given a username (e.g., @cs1511@),
  -> String             -- ^ Convert it to a full course name (e.g.,
                        --   @COMP1511@), using CSE naming conventions.
user_to_class x = prefix ++ code
  where
    (pre, code) = splitAt 2 x
    (Just prefix) = Map.lookup pre (fst prefix_maps)

-- | Convert a full course code to a username.
--
--   >>> class_to_user "COMP1511"
--   "cs1511"
class_to_user
  :: String             -- ^ Given a course code (e.g. @COMP1511@),
  -> String             -- ^ Return (a guess for) the course account,
                        --   according to CSE naming conventions (e.g.,
                        --   @cs1511@)
class_to_user x = pre ++ code
  where
    (prefix, code) = splitAt 4 x
    (Just pre) = Map.lookup prefix (snd prefix_maps)

prefix_maps :: (Map String String, Map String String)
prefix_maps =
  let xs =
        [ ("bi", "BINF")
        , ("cs", "COMP")
        , ("en", "ENGG")
        , ("ge", "GENE")
        , ("gs", "GSOE")
        , ("hs", "HSCH")
        , ("is", "INFS")
        , ("rz", "REGZ")
        , ("se", "SENG")
        ]
      sx = map (swap) xs
  in (Map.fromAscList xs, Map.fromList sx)

------------------------------------------------------------------------

-- | `home` returns our home directory as a canonicalised file path.
home :: IO FilePath
home = home'
#ifdef SAFE_TO_CANONICALISE_PATHS
  >>= canonicalizePath
#endif

-- | `home'` returns @/home/$USER@, our nominal home directory.
--
--   You'll usually want `home` unless you want to manipulate that path
--   and canonicalise later.
home' :: IO FilePath
home' = do
  user_ <- give_user
  return $ "/" </> "home" </> user_


------------------------------------------------------------------------

-- | `web` returns our web directory, with the same canonicalisation
--   caveats that apply to `home`.
web :: IO FilePath
web = web'
#ifdef SAFE_TO_CANONICALISE_PATHS
  >>= canonicalizePath
#endif

-- | `web'` returns @/web/$USER@, our nominal web directory.  You'll
--   usually want `web` unless you want to manipulate that path and
--   canonicalise later.
web' :: IO FilePath
web' = do
  user_ <- give_user
  return $ "/" </> "web" </> user_


------------------------------------------------------------------------

-- | `web` returns our web directory, with the same canonicalisation
-- caveats that apply to `home`.
session_web :: IO FilePath
session_web = session_web'
#ifdef SAFE_TO_CANONICALISE_PATHS
  >>= canonicalizePath
#endif

-- | `session_web'` returns @/web/$USER/$GIVEPERIOD@, our nominal
-- sessional web directory.  You'll usually want `session_web` unless
-- you want to manipulate that path and canonicalise later.
session_web' :: IO FilePath
session_web' = do
  web_ <- web'
  session_ <- give_period
  return $ web_ </> session_


------------------------------------------------------------------------

-- | `work` returns our Give working directory for this session, with
-- the same canonicalisation caveats that apply to `home`.
work :: IO FilePath
work = work'
#ifdef SAFE_TO_CANONICALISE_PATHS
  >>= canonicalizePath
#endif

-- | `work'` returns @/home/$USER/$GIVEPERIOD.work@, our nominal Give
-- working directory.  You'll usually want `work` unless you want to
-- manipulate that path and canonicalise later.
work' :: IO FilePath
work' = fmap fst work''

-- | `smsdb` returns our SMS database for this session, with the same
-- canonicalisation caveats that apply to `home`.
smsdb :: IO FilePath
smsdb = smsdb'
#ifdef SAFE_TO_CANONICALISE_PATHS
  >>= canonicalizePath
#endif

-- | `smsdb'` returns a non-canonical path to the SMS database for
-- this session.
smsdb' :: IO FilePath
smsdb' = fmap snd work''

work'' :: IO (FilePath, FilePath)
work'' = do
  home_ <- home'
  session_ <- give_period
  let work_ = home_ </> session_ <.> "work"
  let smsdb__ = session_ ++ "db" <.> "sms"
  let smsdb_ = work_ </> smsdb__
  return $ (work_, smsdb_)


------------------------------------------------------------------------

-- | `out` returns the output directory for this session, with the
-- same canonicalisation caveats that apply to `home`.
out :: IO FilePath
out = out'
#ifdef SAFE_TO_CANONICALISE_PATHS
  >>= canonicalizePath
#endif

-- | `out'` returns the output directory for this session.
out' :: IO FilePath
out' = do
  work_ <- work'
  return $ work_ </> ".out"


------------------------------------------------------------------------

-- | `marked` returns the marked output directory for this session,
-- with the same canonicalisation caveats that apply to `home`.
marked :: IO FilePath
marked = marked'
#ifdef SAFE_TO_CANONICALISE_PATHS
  >>= canonicalizePath
#endif

-- | `marked'` returns the output directory for this session.
marked' :: IO FilePath
marked' = do
  out_ <- out'
  return $ out_ </> ".marked"


------------------------------------------------------------------------

-- | `assign_dir` returns the work directory for one particular
-- assignment this session, with the same canonicalisation caveats
-- that apply to `home`.
assign_dir :: Text -> IO FilePath
assign_dir assign = assign_dir' assign
#ifdef SAFE_TO_CANONICALISE_PATHS
  >>= canonicalizePath
#endif

-- | `assign_dir'` returns the work directory for one particular
-- assignment this session.
assign_dir' :: Text -> IO FilePath
assign_dir' assign = do
  work_ <- work'
  return $ work_ </> (T.unpack assign)


------------------------------------------------------------------------

give_spec :: FilePath
give_spec = "give.spec"

-- | `global_spec` returns the path to the global give spec.
global_spec :: IO FilePath
global_spec = do
  return $ "" </> "home" </> "give" </> "stable" </> give_spec

-- | `class_spec` returns the path to the class give spec.
class_spec :: IO FilePath
class_spec = do
  home_ <- home'
#ifdef SAFE_TO_CANONICALISE_PATHS
    >>= canonicalizePath
#endif
  return $ home_ </> give_spec

-- | `session_spec` returns the path to this session's root give spec.
session_spec :: IO FilePath
session_spec = do
  work_ <- work'
#ifdef SAFE_TO_CANONICALISE_PATHS
    >>= canonicalizePath
#endif
  return $ work_ </> give_spec

-- | `assign_spec` returns the path to an assignment's give spec.
assign_spec :: Text -> IO FilePath
assign_spec assign = do
  assign_dir_ <- assign_dir' assign
#ifdef SAFE_TO_CANONICALISE_PATHS
    >>= canonicalizePath
#endif
  return $ assign_dir_ </> give_spec


------------------------------------------------------------------------

-- | `adm_root` returns the canonical path to the adm hierachy.
adm :: IO FilePath
adm = adm'
{-- do
  adm_root_ <- adm'
  canonicalizePath $ adm_root_
--}

-- | It turns out that web requests are jailed into @public_html@, and
-- that because mkweb got scrapped, mkgive now happily lives in the
-- sessional web root, so we can just use that.

adm' :: IO FilePath
adm' = session_web'
{--
adm' = do
  home_ <- home'
  session_ <- give_period
  return $ home_ </> session_ <.> "adm"
--}

adm_data :: String -> IO FilePath
adm_data file = do
  adm_root_ <- adm'
  -- canonicalizePath $ adm_root_ </> "_data" </> file
  return $ adm_root_ </> "_data" </> file

adm_activities :: IO FilePath
adm_activities = do
  adm_root_ <- adm'
  -- canonicalizePath $ adm_root_ </> "_activities"
  return $ adm_root_ </> "_activities"


------------------------------------------------------------------------
