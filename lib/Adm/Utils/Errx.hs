{-|
Module:         Adm.Utils.Errx
Description:    Functions like C's @errx(3)@
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

Functions like the C @errx(3)@ family of functions; cause the
program to exit with an error message.
-}

module Adm.Utils.Errx
  ( -- * Error handling
    errx
  , warnx

    -- * 'sysexits.h'
  , Ex(..)
  , ex_to_retval
  , retval_to_ex
  )
where

import           System.Environment (getProgName)
import           System.Exit        (exitWith, ExitCode(ExitFailure))
import           System.IO          (hPutStrLn, stderr)

------------------------------------------------------------------------

-- | @errx@ will terminate the Haskell runtime, ending the program,
--   after printing the name of the program, and the passed error text,
--   like:
--
--   ghci> errx EX_USAGE $ "the program didn't work"
--   <interactive>: the program didn't work
--   *** Exception: ExitFailure 64

errx
   :: Ex                -- ^ For some error value,
   -> String            -- ^ And given a string to be printed to
                        --   the system's standard error handle,
   -> IO ()             -- ^ Return the IO unit.  Actually, that's
                        --   only to satisfy the type system; this
                        --   is actually a bottom-like function as
                        --   it terminates runtime execution.
errx val_ str_ = do
  progn <- getProgName
  hPutStrLn stderr $ progn ++ ": " ++ str_
  exitWith . ExitFailure . ex_to_retval $ val_


-- | @warnx@ will print a warning message.
--
--   At an interactive, for example:
--   ghci> warnx "the program didn't work"
--   <interactive>: the program didn't work
--   it :: ()
warnx
   :: String            -- ^ Given a string to be printed to the
                        --   system's standard error handle,
   -> IO ()             -- ^ Return the IO unit as a result of
                        --   printing that particular string.
warnx str_ = do
  progn <- getProgName
  hPutStrLn stderr $ progn ++ ": " ++ str_

------------------------------------------------------------------------

-- | An enumeration matching the @<sysexits.h>@ values.
data Ex
  = EX_OK
  | EXIT_SUCCESS
  | EXIT_FAILURE
  | EX_USAGE            -- ^ command line usage error (64)
  | EX_DATAERR          -- ^ data format error (65)
  | EX_NOINPUT          -- ^ cannot open input (66)
  | EX_NOUSER           -- ^ addressee unknown (67)
  | EX_NOHOST           -- ^ host name unknown (68)
  | EX_UNAVAILABLE      -- ^ service unavailable (69)
  | EX_SOFTWARE         -- ^ internal software error (70)
  | EX_OSERR            -- ^ system error (e.g., can't fork) (71)
  | EX_OSFILE           -- ^ critical OS file missing (72)
  | EX_CANTCREAT        -- ^ can't create (user) output file (73)
  | EX_IOERR            -- ^ input/output error (74)
  | EX_TEMPFAIL         -- ^ temp failure; user is invited to retry (75)
  | EX_PROTOCOL         -- ^ remote error in protocol (76)
  | EX_NOPERM           -- ^ permission denied (77)
  | EX_CONFIG           -- ^ configuration error (78)
  deriving (Show, Read, Ord, Eq)


-- | Take a 'sysexits.h' name, and return its integer.
ex_to_retval :: Ex -> Int
ex_to_retval EX_OK          = 0
ex_to_retval EXIT_SUCCESS   = 0
ex_to_retval EXIT_FAILURE   = 1
ex_to_retval EX_USAGE       = 64
ex_to_retval EX_DATAERR     = 65
ex_to_retval EX_NOINPUT     = 66
ex_to_retval EX_NOUSER      = 67
ex_to_retval EX_NOHOST      = 68
ex_to_retval EX_UNAVAILABLE = 69
ex_to_retval EX_SOFTWARE    = 70
ex_to_retval EX_OSERR       = 71
ex_to_retval EX_OSFILE      = 72
ex_to_retval EX_CANTCREAT   = 73
ex_to_retval EX_IOERR       = 74
ex_to_retval EX_TEMPFAIL    = 75
ex_to_retval EX_PROTOCOL    = 76
ex_to_retval EX_NOPERM      = 77
ex_to_retval EX_CONFIG      = 78


-- | Take a return value; if it has a 'sysexits.h' mapping, return that.
retval_to_ex :: Int -> Maybe Ex
retval_to_ex 0  = Just EXIT_SUCCESS
retval_to_ex 1  = Just EXIT_FAILURE
retval_to_ex 64 = Just EX_USAGE
retval_to_ex 65 = Just EX_DATAERR
retval_to_ex 66 = Just EX_NOINPUT
retval_to_ex 67 = Just EX_NOUSER
retval_to_ex 68 = Just EX_NOHOST
retval_to_ex 69 = Just EX_UNAVAILABLE
retval_to_ex 70 = Just EX_SOFTWARE
retval_to_ex 71 = Just EX_OSERR
retval_to_ex 72 = Just EX_OSFILE
retval_to_ex 73 = Just EX_CANTCREAT
retval_to_ex 74 = Just EX_IOERR
retval_to_ex 75 = Just EX_TEMPFAIL
retval_to_ex 76 = Just EX_PROTOCOL
retval_to_ex 77 = Just EX_NOPERM
retval_to_ex 78 = Just EX_CONFIG
retval_to_ex _  = Nothing

------------------------------------------------------------------------
