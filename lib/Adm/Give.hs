{-|
Module:         Adm.Give
Description:    An abstraction over Give.
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

`Adm.Give` is an abstraction over Give's rather strange and irregular
syntax, defining instead strongly-typed Haskell structures to describe
spec-files.

By default, we use reasonable behaviours, and attempts to set up a
"normal" class.  We assume a fairly normal SMS database, and assume
the grouping field is @tut_lab@.

We set up submissions so they will follow this tree, for some
sample assignment @assn1@, and one tute-lab, @tue09-strings@,
with one student @z5555555@ who has submitted twice.

@
\$HOME\/
└╴ ${GIVEPERIOD}.work\/
   ├╴ give.spec
   ├╴ ${GIVEPERIOD}db.sms\/
   ├╴ .out\/
   │  ├╴ GROUPS
   │  ├╴ .marked\/
   │  │  ├╴ all\/
   │  │  ├╴ orphans\/
   │  │  └╴ tue09-strings\/
   │  ├╴ all\/
   │  ├╴ orphans\/
   │  └╴ tue09-strings\/
   │     └╴ assn1.5555555
   └╴ assn1\/
      ├╴ give.spec
      ├╴ GROUPS
      ├╴ tests\/
      ├╴ results\/
      ├╴ orphans\/
      └╴ tue09-strings\/
         └╴ 5555555\/
            ├╴ log
            ├╴ !dryrun_record
            ├╴ submission.tar
            └╴ sub0.tar
@

-}
------------------------------------------------------------------------


module Adm.Give
  ( User
  , UPI
  , AssignName

  -- * Give EDSL
  -- ** Directives
  , Directive(..)

  -- ** Specfile directives
  , SpecFile(..)
  , IsGlobalSpec, IsClassSpec, IsSessionSpec, IsAssignSpec

  -- ** Directive Contents
  , TestModeSpec(..)
  , DeadlineSpec(..)
  , deadline_spec_from_text
  , AuthSpec(..)

  , GiveSpecial(..)

  -- ** Give options.
  , GiveOptions(..)

  -- * Give syntactic structures
  , directive, comment, set, block_directive
  , default_session_spec
  )
where

import           Data.Aeson
import           Data.Map (Map)
import qualified Data.Map as Map
import           Data.Maybe
import           Data.Text (Text)
import qualified Data.Text as T
import           Data.Time
import           GHC.Generics

import           Adm.Types

------------------------------------------------------------------------

-- | The name of an assignment.
type AssignName = Text

-- | Here's how we handle user permissions in Give.
data AuthSpec = AuthSpec
  { auth_user :: User
  , can_mark :: Bool
  , can_rsms :: Bool
  }
deriving instance Show AuthSpec
deriving instance Eq AuthSpec
deriving instance Generic AuthSpec

-- | What mode should we do testing in?
data TestModeSpec
  = NoTests
    -- ^ The submission will not be subjected to a performance test
    -- sequence, although both objective and subjective marks can still
    -- be awarded if desired.  Most often used with textual material
    -- that is examined by a human marker, or where an objective mark
    -- makes up the entire assessment.
  | Advisory
    -- ^ The submission will be subjected to one or more tests, the
    -- results of which will be examined by a human marker and marks
    -- awarded accordingly.
  | Automark
    -- ^ The submission will be subjected to one or more tests, and
    -- marks awarded on the basis of objective analysis of the results.
  | Deferred
    -- ^ The submission will be subjected to one or more tests, but
    -- these will be deferred until the human marker is present.  The
    -- marker will decide whether each test is passed, failed or cannot
    -- be determined (which equates to a conceded pass).  In other
    -- respects, the `Deferred` mode is like `Automark`.

deriving instance Show TestModeSpec
deriving instance Eq TestModeSpec
deriving instance Generic TestModeSpec

-- | The contrived mess of deadline specs.
data DeadlineSpec = DeadlineSpec
  { deadline_group :: Text
  , submit_by :: UTCTime
  , submit_close :: Maybe UTCTime
  , submit_open :: Maybe UTCTime
  }
deriving instance Show DeadlineSpec
deriving instance Eq DeadlineSpec
deriving instance Generic DeadlineSpec

instance FromJSON DeadlineSpec where
  parseJSON = withObject "DeadlineSpec" $
    \v -> (deadline_spec_from_text "all") <$>
          v .: "due" <*>
          v .:? "end" <*>
          v .:? "open"
    where

deadline_spec_from_text
  :: Text -> Text -> Maybe Text -> Maybe Text
  -> DeadlineSpec
deadline_spec_from_text grp due end open =
  DeadlineSpec grp
    (strptime due)
    (fmap strptime $ end)
    (fmap strptime $ open)
  where
    strptime' =
      (parseTimeOrError True
       defaultTimeLocale "%Y-%m-%d %H:%M:%S")
    strptime = strptime' . T.unpack

data GiveSpecial = GiveSpecial
  { special_mark :: Float
  , special_descr :: Text
  , special_script :: Text
  }
deriving instance Show GiveSpecial
deriving instance Eq GiveSpecial
deriving instance Generic GiveSpecial

instance FromJSON GiveSpecial where
  parseJSON = withObject "GiveSpecial" $
    \v -> GiveSpecial
    <$> v .: "mark"
    <*> v .: "descr"
    <*> v .: "script"

------------------------------------------------------------------------

-- | Type-level Boolean negation
type family (~~~) (b :: Bool) :: Bool
type instance (~~~) 'True  = 'False
type instance (~~~) 'False = 'True

-- | Type-level Boolean 'or'
type family (a :: Bool) ||| (b :: Bool) :: Bool
type instance 'False ||| 'False = 'False
type instance a      ||| 'True  = 'True
type instance 'True  ||| b      = 'True

-- | Type-level Boolean 'and'
type family (a :: Bool) &&& (b :: Bool) :: Bool
type instance 'True  &&& 'True  = 'True
type instance 'False &&& b      = 'False
type instance a      &&& 'False = 'False

-- | A phantom type used to annotate a 'Directive' instance with the
-- context in which it is occurring.
data SpecFile = GlobalSpec | ClassSpec | SessionSpec | AssignSpec

-- | Type-family constraining all phantom SpecFile directives.
type family IsSpecFile a where
  IsSpecFile 'GlobalSpec  = 'True
  IsSpecFile 'ClassSpec   = 'True
  IsSpecFile 'SessionSpec = 'True
  IsSpecFile 'AssignSpec  = 'True
  IsSpecFile a            = 'False

-- | Type-family constraining the phantom annotation GlobalSpec.
type family IsGlobalSpec a where
  IsGlobalSpec 'GlobalSpec = 'True
  IsGlobalSpec a           = 'False

-- | Type-family constraining the phantom annotation ClassSpec.
type family IsClassSpec a where
  IsClassSpec 'ClassSpec = 'True
  IsClassSpec a          = 'False

-- | Type-family constraining the phantom annotation SessionSpec.
type family IsSessionSpec a where
  IsSessionSpec 'SessionSpec = 'True
  IsSessionSpec a            = 'False

-- | Type-family constraining the phantom annotation AssignSpec.
type family IsAssignSpec a where
  IsAssignSpec 'AssignSpec = 'True
  IsAssignSpec a           = 'False

------------------------------------------------------------------------

-- | Directives form the majority of the @give.spec@ file.
data Directive d where
  -- | The current session, in CSE's (unique!) @/YY/s/S/@ convention:
  -- s1 = session 1, s2 = session 2, x1 = summer session.
  -- Typically defined in the global give.spec;
  -- often overridden in a class give.spec.
  --
  -- @
  -- &period = 18s1
  -- @
  --
  Period
    :: (IsGlobalSpec d ||| IsClassSpec d) ~ 'True
    => Text -> Directive d

  -- | Do we accept submissions from students not known to the course?
  -- These assignments wind up in the magic subgroup @orphans@.
  --
  -- This tends to cover specifically students known to SMS, so course
  -- staff (e.g., tutors, lecturers) tend to be in this bucket, and thus
  -- it's a good idea to leave this on.
  --
  -- Typically defined in the global give.spec.
  -- Potentially overridden in a class' or a session's give.spec.
  --
  -- @
  -- &acceptorphans = TRUE
  -- @
  --
  AcceptOrphans
    :: IsSpecFile d ~ 'True
    => Bool -> Directive d

  -- | How many backups we should keep for a submission; by default,
  -- we keep 1, though there may be good reason to set this higher.
  --
  -- An alternative approach to keeping backups of submissions might be
  -- a @postsub@ hook into a proper revision control system like Git, as
  -- Andrew Taylor does with COMP2041 and COMP1511.
  --
  -- Defined in the global give.spec.
  --
  -- @
  -- &backups = 1
  -- @
  Backups
    :: IsSpecFile d ~ 'True
    => Int -> Directive d

  -- | The full name of the course.
  -- Defined in the course account give.spec.
  -- Probably shouldn't be overridden by other specs.
  --
  -- @
  -- &class = COMP1511
  -- @
  Class
    :: (IsClassSpec d) ~ 'True
    => Text -> Directive d

  -- | The course account.
  -- Defined in the course account give.spec.
  -- Probably shouldn't be overridden by other specs.
  --
  -- @
  -- &classuser = cs1511
  -- @
  ClassUser
    :: (IsClassSpec d) ~ 'True
    => User -> Directive d

  -- | An entry in the login map is a login/student-ID pair.  The
  -- default session give.spec suggests this is only needed for
  -- 'unofficial' mappings, and is automatic for normal students
  -- with real logins.
  --
  -- Defined in the session give.spec.
  -- Probably shouldn't be overridden by other specs.
  --
  -- (... but if you _actually_ want to become a user from the
  -- class account, you want @classrun -u STU-ID@ instead.)
  --
  -- @
  -- &loginmap
  -- andrewt 9300162
  -- &end
  -- @
  LoginMap
    :: (IsSessionSpec d) ~ 'True
    => [(User, UPI)] -> Directive d

  -- | The authorisation map is a (user,MARK,RSMS) triple.
  -- This turns on Xmark and rSMS for tutors to mark work and enter
  -- grades; you can restrict this to various users and combinations.
  --
  -- Xmark is still with us, though you can hack around it.
  -- rSMS has mostly been replaced by WebCMS 3 or SMS.py.
  --
  -- Defined in the session give.spec.
  -- Probably shouldn't be overridden by other specs.
  --
  -- @
  -- &authorisation
  -- andrewt MARK RSMS
  -- &end
  -- @
  Authorisation
    :: (IsSessionSpec d) ~ 'True
    => [AuthSpec] -> Directive d

  -- | The location of the SMS database for this session.
  -- Defined in the session give.spec.
  -- Probably shouldn't be overridden by other specs.
  --
  -- @
  -- &smsdb = \/home\/cs1511\/18s1.work\/18s1db.sms
  -- @
  SMSDB
    :: (IsSessionSpec d) ~ 'True
    => FilePath -> Directive d

  -- | The second level of grouping in the submission hierarchy.
  --
  -- Typical submissions have @sms tut_lab@ to group submissions
  -- by class; exams, etc. sometimes use @none@, which dumps all
  -- submissions in group @all@.
  --
  -- Defined in the session give.spec.
  -- If needed, overridden in per-assignment specs.
  --
  -- @
  -- &subgroup = sms tut_lab
  -- @
  SubGroup
    :: (IsSessionSpec d ||| IsAssignSpec d) ~ 'True
    => Text -> Directive d

  -- | The third level of grouping in the submission hierarchy.
  --
  -- Typical submissions use @stuID@ to use the student's zID or
  -- UPI (UNSW/unique? person identifier).  For group projects,
  -- this can be overridden to make submissions apply to several
  -- students based on a secondary index in the SMS database
  -- (e.g., @sms lab_pair_2@).
  --
  -- Defined in the session give.spec.
  -- If needed, overridden in per-assignment specs.
  --
  -- @
  -- &subkey = stuID
  -- &subkey = sms lab_pairs_2
  -- @
  SubKey
    :: (IsSessionSpec d ||| IsAssignSpec d) ~ 'True
    => Text -> Directive d

  -- | Options passed to @tar@ when creating a submission tarball.
  -- Defined in the global give.spec.
  -- Probably doesn't need to be overridden.
  --
  -- @
  -- &suboptions = z
  -- @
  SubOptions
    :: IsSpecFile d ~ 'True
    => Text -> Directive d

  -- | List of assignments in this session.
  -- Defined in the session give.spec.
  --
  -- @
  -- &assignmap
  -- ass1
  -- ass2
  -- &end
  -- @
  AssignMap
    :: (IsSessionSpec d) ~ 'True
    => [AssignName] -> Directive d

  -- | List of assignments that cannot be collected by students.
  -- Must be a subset of the assignments that exist.
  --
  -- Defined in the session give.spec.
  --
  -- @
  -- &withhold
  -- final_exam_part1_q1
  -- &end
  -- @
  Withhold
    :: (IsSessionSpec d) ~ 'True
    => [AssignName] -> Directive d

  -- | The name of this assignment.
  --
  -- Defined in the assignment give.spec.
  --
  -- @
  -- &assign = wk01_kangaroo
  -- @
  Assign
    :: (IsAssignSpec d) ~ 'True
    => AssignName -> Directive d

  -- | The name of the SMS field this assignment produces marks for,
  -- if it's not the name specified by "Assign".
  --
  -- Defined in the assignment give.spec.
  --
  -- @
  -- &smsmark = wk01_kangaroo
  -- @
  SMSMark
    :: (IsAssignSpec d) ~ 'True
    => Text -> Directive d

  -- | Synonymous with @SMSMark@.
  -- Defined in the assignment give.spec.
  SMSGrade
    :: (IsAssignSpec d) ~ 'True
    => Text -> Directive d

  -- | The maximum size of a submission in kilobytes,
  -- or zero for no limit.
  --
  -- Defined in the global give.spec; can be overridden.
  --
  -- @
  -- &maxsize = 128
  -- @
  MaxSize
    :: IsSpecFile d ~ 'True
    => Int -> Directive d

  -- | The deadline for each group.  See "DeadlineSpec".
  --
  -- Defined in the assignment give.spec.
  --
  -- @
  -- &deadline
  -- all Sun 30 Jul 23:59:59, Mon 24 Jul 00:00:00, Sun 06 Aug 23:59:59
  -- &end
  -- @
  Deadline
    :: (IsAssignSpec d) ~ 'True
    => [DeadlineSpec] -> Directive d

  -- | Extensions have a very simple syntax of user-to-extension;
  -- typically, this is specified in days.
  --
  -- Defined in the assignment give.spec.
  --
  -- @
  -- &extension
  -- 5017851    4 days
  -- &end
  -- @
  Extension
    :: (IsAssignSpec d) ~ 'True
    => [(User, Text)] -> Directive d

  -- | Define a late penalty. Defined in the assignment give.spec.
  --
  -- Has a really scary syntax. Originally unimplemented here
  -- because the Tcl that understood it (!) was nightmarish.
  --
  -- @
  -- &latepen
  -- ceiling % 0 per hour for 2 hours
  -- ceiling % 2 per hour for 50 hours
  -- &end
  -- @
  LatePenalty
    :: (IsAssignSpec d) ~ 'True
    => [Text] -> Directive d

  -- | Like "LatePenalty", but the opposite.
  -- Defined in the assignment give.spec.
  EarlyBonus
    :: (IsAssignSpec d) ~ 'True
    => [Text] -> Directive d

  -- | Describes the files that can be submitted for an assignment;
  -- defined in the assignment give.spec.
  --
  -- Has a really scary syntax; originally unimplemented here
  -- because the Tcl that understood it (!) was nightmarish.
  --
  -- @
  -- &subspecs
  -- accept  Makefile dracula.c hunter.c *.c\/0+ *.h\/0+
  -- &end
  -- @
  Submission
    :: (IsAssignSpec d) ~ 'True
    => [Text] -> Directive d

  -- | Delimits the section of the file where we define test run
  -- specifications; defined in the assignment give.spec.
  --
  -- @
  -- &runspecs
  -- @
  RunSpecs
    :: (IsAssignSpec d) ~ 'True
    => Directive d

  -- | What mode should tests be run in?
  -- See `TestModeSpec` for details of how this is defined.
  -- Defined in the assignment give.spec.
  --
  -- @
  -- &testmode automark
  -- @
  TestMode
    :: (IsAssignSpec d) ~ 'True
    => TestModeSpec -> Directive d

  -- | Define a Bourne shell script that will be run once at the
  -- beginning of the test sequence, which can be used (as the name
  -- suggests) to compile student submissions for testing.
  --
  -- Defined in the assignment give.spec.
  Compile
    :: (IsAssignSpec d) ~ 'True
    => [Text] -> Directive d

  -- | Define a set of subjective marks.
  --
  -- In the spec-file, this is a block directive of label/mark pairs,
  -- separated by tabs; the block header has the sum of the marks to be
  -- awarded.
  --
  -- Defined in the assignment give.spec.
  --
  -- @
  -- &subjective	2
  -- Commenting	1
  -- Code layout	1
  -- &end
  -- @
  SubjectiveMark
    :: (IsAssignSpec d) ~ 'True
    => [(Text, Float)] -> Directive d

  -- | Define a Bourne shell script that will be run once towards the
  -- end of the test sequence, which can be used to run tests that
  -- aren't normally defined.  The script must set the environment
  -- variable @$MARK@ to the value to be awareded.
  --
  -- It's not clear what would happen if multiple special-test sections
  -- are defined.
  --
  -- Defined in the assignment give.spec.
  --
  -- @
  -- &special	9	Correctness
  -- TOT=$(grep -c Passed $S\/testing.out)
  -- MARK=$(echo $TOT | awk '{printf "%0.1f",9.0*$1\/18}')
  -- &end
  -- @
  SpecialTest
    :: (IsAssignSpec d) ~ 'True
    => (Float, Text) -> [Text] -> Directive d

  -- | Set a grade for an assessment.
  OverallGrade
    :: (IsAssignSpec d) ~ 'True
    => [Text] -> Directive d

  -- | Set the total mark for an assessment.
  TotalMark
    :: (IsAssignSpec d) ~ 'True
    => Float -> Directive d

-- deriving instance Generic (Directive d)
deriving instance Eq (Directive d)
instance Show (Directive d) where
  show x =
    T.unpack $ show_directive x

------------------------------------------------------------------------

show_directive :: (Directive d) -> Text
show_directive (Period x)          = set "period" x
show_directive (AcceptOrphans x)   = set "acceptorphans" $
                                     T.toUpper . showT $ x
show_directive (Backups x)         = set "backups" $ showT x
show_directive (Class x)           = set "class" x
show_directive (ClassUser x)       = set "classuser" x
show_directive (SMSDB x)           = set "smsdb" $ T.pack x
show_directive (SMSMark x)         = set "smsmark" x
show_directive (SMSGrade x)        = set "smsgrade" x
show_directive (SubKey x)          = set "subkey" x
show_directive (SubGroup x)        = set "subgroup" x
show_directive (SubOptions x)      = set "suboptions" x
show_directive (MaxSize x)         = set "maxsize" $ showT x
show_directive (Assign x)          = set "assign" x

show_directive (LoginMap xs)       = block_of_lines "loginmap" $
                                     map (lm_one_entry) xs
  where lm_one_entry (u, s) = untabs [u, showT s]

show_directive (Authorisation xs)  = block_of_lines "authorisation" $
                                     map (am_one) xs
  where am_one (AuthSpec user_ mark rsms)
                                   = T.unwords [user_, mark_, rsms_]
          where
            mark_ = γ "MARK" mark
            rsms_ = γ "RSMS" rsms
            γ x True  = x
            γ _ False = ""

show_directive (AssignMap xs)      = block_of_lines "assignmap" xs
show_directive (Withhold xs)       = block_of_lines "withhold" xs
show_directive (Deadline xs)       = block_of_lines "deadline" $
                                     map (show_deadline) xs
  where
        -- A contrived hack to show deadlines in a form that Give likes.
        --
        -- Haskell has effectively no locale support.  This problem is
        -- so obviously bad that the @time@ package provides a "default"
        -- locale; to a rough approximation, it's American.  There's
        -- also no nice way to load arbitrary locales, so...
        show_deadline (DeadlineSpec group by_ close open) =
          T.concat $
          [ group, "\t"
          , T.pack $ deadline_to_s $ by_
          , T.pack $ show_deadline' $ open
          , T.pack $ show_deadline' $ close
          ]
        show_deadline' (Just x) = ", " ++ (deadline_to_s x)
        show_deadline' Nothing  = ""
        deadline_to_s = formatTime defaultTimeLocale "%a %d %b %Y %H:%M:%S"
show_directive (Extension xs)      = block_of_lines "extension" $
                                     map (\(a, b) -> T.unwords [a, b]) $ xs
show_directive (EarlyBonus xs)     = block_of_lines "earlybonus" xs
show_directive (LatePenalty xs)    = block_of_lines "latepen" xs

show_directive (Submission xs)     = block_of_lines "subspecs" xs
show_directive (Compile xs)        = block_of_lines "compile" xs
show_directive (SpecialTest (m, s) xs)
                                   = block_of_lines special $ xs
  where special = T.unwords ["special", showT m, s]
show_directive (SubjectiveMark xs) = block_of_lines subjective $ xs'
  where subjective = T.unwords ["subjective", showT sum_mark]
        xs' = map (\(a, b) -> untabs [a, showT b]) $ xs
        sum_mark = sum . map snd $ xs

show_directive (RunSpecs)          = directive "runspecs"

show_directive (TestMode x)        = directive testmode
  where testmode = T.unwords ["testmode", show_tms x]
        show_tms NoTests  = "notests"
        show_tms Advisory = "advisory"
        show_tms Automark = "automark"
        show_tms Deferred = "deferred"

show_directive (OverallGrade xs)   = directive grade
  where grade = T.unwords ["overallgrade", T.intercalate "," xs]

show_directive (TotalMark x)       = directive totalmark
  where totalmark = T.unwords ["totalmark", showT x]

showT :: Show a => a -> Text
showT = T.pack . show

block_of_lines :: Text -> [Text] -> Text
block_of_lines d xs = block_directive d $ T.intercalate "\n" $ xs

untabs :: [Text] -> Text
untabs = T.intercalate "\t"

------------------------------------------------------------------------

data GiveOptions = GiveOptions
  { late_penalty :: Maybe [Text]
  , extension :: Map User Text
  , give_compile :: Maybe Text
  , give_subjective :: Maybe (Map Text Float)
  , give_special :: Maybe [GiveSpecial]
  }
deriving instance Show GiveOptions
deriving instance Eq GiveOptions
deriving instance Generic GiveOptions

instance FromJSON GiveOptions where
  parseJSON = withObject "GiveOptions" $
    \v -> maybe_give_options
    <$> v .:? "late-penalty"
    <*> v .:? "extensions"
    <*> v .:? "compile"
    <*> v .:? "subjective"
    <*> v .:? "special"
    where
      maybe_give_options _1 _2 _3 _4 _5 =
        GiveOptions _1 (fromMaybe Map.empty $ _2) _3 _4 _5

------------------------------------------------------------------------
--
-- Give's spec syntax is quite particular: most notable is the magic
-- control sequence '&', but also the comment syntax, '&#', and the
-- magical setter directives.  So, we syntactic sugar these away.

-- | Emit a Give directive of the form @&d@.
directive :: Text -> Text
directive d = T.concat ["&", d]

-- | Emit a Give comment.
comment :: Text -> Text
comment c = directive $ T.concat ["# ", c]

-- | Emit a Give directive of the form @&k = v@.
set :: Text -> Text -> Text
set k v = directive $ T.concat [k, " = ", v]

-- | Emit a "block" directive @&d@ ... @&end@.
block_directive :: Text -> Text -> Text
block_directive d blk = T.unlines [head_, blk', tail_]
  where head_ = directive d
        blk'  = if (not . T.null) blk then blk else ""
        tail_ = directive "end"

------------------------------------------------------------------------

default_session_spec :: [Directive 'SessionSpec]
default_session_spec =
  [ LoginMap []
  , {- Authorisation []
  , SMSDB "db.sms"
  , -} SubOptions "z"
  , SubKey "stuID"
  , SubGroup "sms tut_lab"
  , Withhold []
  , AssignMap []
  ]
