{-|
Module:         Adm.Autotest
Description:    automated testing of code
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

Describe a set of automatic tests to be run.

-}
------------------------------------------------------------------------


module Adm.Autotest
  ( Options(..)
  , test_statistics
  , load_bytes
  , get_activity_tmpdir
  , get_autotest_tmpdir
  , set_up_tmpdir

  -- * The Autotest type
  , Autotest(..)

  -- * Associated types
  , Fixture(..)
  , CompileStatus(..)
  , TestRunStatus(..)
  , TestResult(..)
  , TestStatus(..)
  , CompileWith(..)
  , RunWith(..)
  )
where

import           Data.Aeson hiding (Options)
import qualified Data.ByteString as B
import           Data.List
import           Data.Maybe
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.Encoding as TE
import           GHC.Generics hiding (from)
import           System.Directory
import           System.Exit
import           System.FilePath
import           System.Posix.Process

import           Adm.Activity
import qualified Adm.Utils.Paths as Paths


------------------------------------------------------------------------

data Autotest = Autotest
  { descr :: Text
  , argv :: Maybe [Text]
  , f_stdin :: Maybe Fixture
  , f_stdout :: Maybe Fixture
  , f_stderr :: Maybe Fixture
  , exit_code :: Maybe Int
  , time_limit :: Maybe Int
  , compile_flags :: Maybe [Text]
  } deriving (Show, Eq, Generic)
instance FromJSON Autotest where
  parseJSON = withObject "Autotest" $
    \v -> Autotest
    <$> v .:  "descr"
    <*> v .:? "argv"
    <*> v .:? "stdin"
    <*> v .:? "stdout"
    <*> v .:? "stderr"
    <*> v .:? "exit_code"
    <*> v .:? "time_limit"
    <*> v .:? "compile_flags"

data Fixture
  = Immediate Text
  | Indirect Text
  deriving (Eq, Generic)
instance Show Fixture where
  show (Immediate t) =
    "[is] " ++ (show t)
  show (Indirect t) =
    "[from] " ++ (show t)
instance FromJSON Fixture where
  parseJSON = withObject "Fixture" $ \v -> do
    fixture_from_toml
      <$> v .:? "is"
      <*> v .:? "from"

fixture_from_toml :: Maybe Text -> Maybe Text -> Fixture
fixture_from_toml (Just is) Nothing     = Immediate is
fixture_from_toml Nothing   (Just from) = Indirect from
fixture_from_toml _         _           =
  error $ "both 'is' and 'from' specified in Fixture"

data TestStatus = TestStatus
  { compiled :: CompileStatus
  , executed :: TestRunStatus
  , expected :: Maybe TestResult
  , actual :: Maybe TestResult
  } deriving (Eq, Show, Generic)

data TestResult = TestResult
  { test_stdout :: B.ByteString
  , test_stderr :: B.ByteString
  , test_exit_val :: ExitCode
  } deriving (Eq, Show, Generic)

data CompileStatus
  = CompileOk
  | CompileFailed
  deriving (Eq, Ord, Generic)
instance Show CompileStatus where
  show CompileOk = "compile ok"
  show CompileFailed = "compile failure"

data TestRunStatus
  = TestPassed
  | TestFailed
  | TestError
  deriving (Eq, Ord, Generic)
instance Show TestRunStatus where
  show TestPassed = "test passed"
  show TestFailed = "test failed"
  show TestError = "test error"

------------------------------------------------------------------------

class CompileWith a where
  do_compile
    :: a
    -> FilePath
    -> String
    -> Activity
    -> Autotest
    -> IO CompileStatus

class RunWith a where
  do_run
    :: a
    -> FilePath
    -> Activity
    -> Autotest
    -> IO TestResult

------------------------------------------------------------------------

data Options = Options
  { old_pwd :: Maybe FilePath
  , parallel :: Bool
  , colorize :: Bool
  , show_input :: Bool
  , show_expected :: Bool
  , show_actual :: Bool
  , show_diff :: Bool
  , show_command :: Bool
  , fail_stderr :: Bool
  , show_exit_value :: Bool
  , keep_artefacts :: Bool
  , autotest_ :: String
  }


------------------------------------------------------------------------

-- | Get me some statistics.
test_statistics :: [TestStatus] -> (Int, Int, Int, Int)
test_statistics tests =
  let count_tests stat =
        length $
        filter ((==) stat) $
        map (executed) $ tests
      n_tests = length tests
      tests_passed = count_tests TestPassed
      tests_failed = count_tests TestFailed
      tests_broken = count_tests TestError
  in  (n_tests, tests_passed, tests_failed, tests_broken)


------------------------------------------------------------------------

-- | Load a fixture, as a ByteString.
--
-- * if the fixture is present and flagged as an immediate, uplifts
--   its Text as a ByteString;
-- * if the fixture is present and flagged as an indirect fixture,
--   load it as a ByteString; or
-- * if the fixture is absent, present the empty ByteString.
load_bytes :: Activity -> Maybe Fixture -> IO B.ByteString
load_bytes act fixt =
  case fixt of
    Just (Immediate t) -> do
      return $ TE.encodeUtf8 t

    Just (Indirect fn) -> do
      act_root <- Paths.adm_activities
      let act_name = T.unpack $ name act
      let act_dir = act_root </> act_name
      B.readFile (act_dir </> T.unpack fn)

    Nothing -> do
      return $ B.empty

------------------------------------------------------------------------

-- | Get the name of a temporary directory to work in.
--   This should wind up with a hierachy that looks a little like:
--
--     $TMPDIR
--       autotest.${activity}.$$/
--         ${test_bin}/
--           ${accept[]}
--           ${test_bin}
--           ${fixture[]}
get_activity_tmpdir :: Activity -> IO FilePath
get_activity_tmpdir act = do
  tmpdir_ <- getTemporaryDirectory
  pid <- fmap show $ getProcessID
  let activity_name = T.unpack $ activity act
  let dirname_parts = [ "autotest", activity_name, pid ]
  return $ tmpdir_ </> intercalate "." dirname_parts

get_autotest_tmpdir :: Activity -> Autotest -> IO FilePath
get_autotest_tmpdir act test = do
  activ_tmpdir <- get_activity_tmpdir act
  let test_bin = T.unpack $ descr test
  return $ activ_tmpdir </> test_bin

set_up_tmpdir :: FilePath -> Activity -> Autotest -> IO FilePath
set_up_tmpdir oldpwd act test = do
  tmpdir <- get_autotest_tmpdir act test
  createDirectoryIfMissing True $ tmpdir

  let (Just act_dir) = act_directory act
  let provides =
        prefix_all act_dir $ just_list $ provide act
  -- warnx $ "copying provided " ++ (intercalate "," provides)
  mapM_ (copy_file_to tmpdir) $ provides

  let accepts =
        prefix_all oldpwd $ accept act
  -- warnx $ "copying provided " ++ (intercalate "," accepts)
  mapM_ (copy_file_to tmpdir) $ accepts

  return $ tmpdir
  where
    prefix_all x = map ((x </>) . T.unpack)
    just_list = fromMaybe []
    copy_file_to dst' src = do
      let dst = dst' </> takeFileName src
      -- warnx $ "cp -p '" ++ src ++ "' '" ++ dst ++ "'"
      copyFile{-WithMetadata-} src dst


------------------------------------------------------------------------

