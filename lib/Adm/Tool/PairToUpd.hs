{-|
Module:         Adm.Tool.Pair
Description:    dump lab pairs as a SMS update file
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

The @pair2upd@ tool
converts a lab pair file,
stored as TOML,
into a SMS update file,
targetting the specified SMS field.

=== Options

 - @--version@:
   Show version information.

 - @-h@, @--help@:
   Show online help text.

 - @/PAIR-FILE/@:
   A TOML-formatted pair file,
   relative to the admin data directory
   (@\/web\/cs1511\/17s2\/_data@).

 - @/SMS-FIELD/@:
   A valid SMS field name.

=== Environment

@pair2upd@ relies on @$CLASS@, @$USER@, and @$GIVEPERIOD@
to determine the location of the admin data directory.

-}


module Adm.Tool.PairToUpd (main)
where

import qualified Data.Map as Map
import           Data.Semigroup ((<>))
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import           Options.Applicative ( Parser
                                     , metavar, argument, str
                                     , help, execParser )

import           Adm.Course.Pairs
import           Adm.Give
import qualified Adm.Utils.Paths as Paths
import           Adm.Utils.Tool (Tool(..), getopt)

------------------------------------------------------------------------

_TOOL :: Tool
_TOOL = Tool
  { tool_name = "pair2upd"
  , tool_summary = "pair file to updates"
  , tool_descr = "Convert a pairs.toml file into a SMS update."
  , tool_hint = "PuzzleQuest?  What PuzzleQuest?"
  }

data Options = Options
  { file :: FilePath
  , field :: String
  } deriving (Show)

getopt' :: Parser Options
getopt' = Options
  -- A list of activities to load.
  <$> (argument str
     ( metavar "PAIR-FILE"
    <> help "a pair file name"
     ))
  <*> (argument str
     ( metavar "SMS-FIELD"
    <> help "the pair SMS field"
     ))

------------------------------------------------------------------------

main :: IO ()
main = do
  opts <- execParser (getopt _TOOL getopt')

  pair_file <- Paths.adm_data $ file opts
  pair_list <- load_pairs_list pair_file
  output_pairs opts pair_list

output_pairs :: Options -> PairMap -> IO ()
output_pairs opts pm =
  TIO.putStr . T.unlines . pair_upd $ Map.toList pm
  where
    pair_upd :: [Pair] -> [Text]
    pair_upd =
      concat . (map output_one_pair)
    output_one_pair :: Pair -> [Text]
    output_one_pair (p,xs) =
      map (output_one_user p) xs
    output_one_user :: PairK -> User -> Text
    output_one_user p x =
      let upi = T.drop 1 x
          field' = T.pack $ field opts
      in  T.concat [ upi, "|", field', "|", p ]

------------------------------------------------------------------------

