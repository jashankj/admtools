{-|
Module:         Adm.Tool.Pair
Description:    list the current user's lab pair
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX
-}


module Adm.Tool.Pair
  ( main
  , find_in_pairs_list
  , report_on_lab_pair )
where

import           Control.Monad
import           Data.Maybe
import qualified Data.Map as Map
import qualified Data.Text as T
import qualified Data.Text.IO as TIO

import           Adm.Course.Pairs
import           Adm.Give
import           Adm.Utils.Errx
import qualified Adm.Utils.Paths as Paths
import           Adm.Utils.Tool (Tool(..))
import           Adm.Utils.Udb

------------------------------------------------------------------------

_TOOL :: Tool
_TOOL = Tool
  { tool_name = "pair"
  , tool_summary = "list the current user's lab pair"
  , tool_descr = ""
  , tool_hint = "PuzzleQuest, uh... finds a way."
  }

main :: IO ()
main = do
  -- TODO which file should be from Adm.Course.Config
  pairs_file <- Paths.adm_data "pairs-3.toml"
  pairs <- load_pairs_list pairs_file
  user_ <- fmap (\u -> T.concat [ "z", T.pack $ show $ user_upi u ])
           $ accSelf

  let maybe_pair = find_in_pairs_list user_ pairs
  when (isNothing maybe_pair)
    (errx EX_NOUSER $
     T.unpack . T.concat $
       [ "User '", user_, "' has no known lab pair. "
       , "Please contact your tutor."
       ])
  report_on_lab_pair $ fromJust maybe_pair

find_in_pairs_list :: User -> PairMap -> Maybe Pair
find_in_pairs_list u pm =
  if not . Map.null $ l
  then Just $ head . Map.toList $ l
  else Nothing
  where
    l = Map.filter (\e -> u `elem` e) pm

report_on_lab_pair :: Pair -> IO ()
report_on_lab_pair (pair_name, users_) = do
  users <- mapM (acc . Just) users_
  let idents =
        map (\u ->
                T.concat [ user_name u
                         , " (z", T.pack $ show $ user_upi u, ")"
                         , " <", user_unswmail u, ">" ]) users
  TIO.putStrLn $ T.concat $
    [ "Found lab pair '", pair_name, "':\n"
    ] ++ (map (\x -> T.concat ["    ", x, "\n"]) idents)


------------------------------------------------------------------------
