{-|
Module:         Adm.Tool.MkGive
Description:    regenerate Give files
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

`mkgive` regenerates
specification files for Give,
and, where relevant,
triggers regeneration and rerunning.

`mkgive` will only generate
necessary Give spec-files;
it will not, for technical reasons,
generate tests, or students.

-}


module Adm.Tool.MkGive (main)
where

import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import           System.Directory
import           System.FilePath
import           System.Posix.Files

import           Adm.Autotest ()
import           Adm.Activity (Activity, activity)
import qualified Adm.Activity as Activity
import qualified Adm.Course as Course
import           Adm.Give ()
import qualified Adm.Give.Assign as GAssign
import qualified Adm.Give.Session as GSession
import qualified Adm.Utils.Paths as Paths
import           Adm.Utils.Tool (Tool(..))

------------------------------------------------------------------------

_TOOL :: Tool
_TOOL = Tool
  { tool_name = "mkgive"
  , tool_summary = "regenerate Give files"
  , tool_descr = ""
  , tool_hint = "PuzzleQuest in Boots."
  }

main :: IO ()
main = do
  activities <- Activity.load_all
  groups <- Course.groups
  GSession.make_session_spec activities
  make_all_assign_specs activities
  make_group_files activities groups
  make_dryruns activities

------------------------------------------------------------------------

make_all_assign_specs :: [Activity] -> IO ()
make_all_assign_specs acts = do
  mapM_ GAssign.make_assign_spec $ acts


------------------------------------------------------------------------
-- Create group files and directories.

make_group_files :: [Activity] -> [Text] -> IO ()
make_group_files acts groups = do
  mapM_ (flip make_assign_groups groups) $ acts

make_assign_groups :: Activity -> [Text] -> IO ()
make_assign_groups act groups = do
  let act_dir = T.pack $ Activity.give_name act
  assign_give_dir <- Paths.assign_dir act_dir
  let groups' = map T.unpack $ groups

  -- Create the directories we need.
  let dirs = map (assign_give_dir </>) $ groups'
  mapM_ (createDirectoryIfMissing True) dirs

  -- Write out the `GROUPS` file.
  let assign_groups = assign_give_dir </> "GROUPS"
  TIO.writeFile assign_groups $
    T.unwords groups

  -- TODO deal with exam submissions in group 'all'.

------------------------------------------------------------------------

-- A `dryrun` file takes two arguments:
--  - a submission tarball, and
--  - the give name.
--
-- A dryrun session starts in a temporary directory anyway!  Hooray.
--
-- TODO move to Adm.Give.Dryrun or similar

make_dryruns :: [Activity] -> IO ()
make_dryruns acts = do
  mapM_ make_one_dryrun $ acts
  where
    make_one_dryrun act = do
      let act_dir = T.pack $ Activity.give_name act
      assign_give_dir <- Paths.assign_dir act_dir
      let dryrun_file = assign_give_dir </> "dryrun"
      TIO.writeFile dryrun_file $ T.unlines $
        [ "#!/bin/sh"
        , "# COMP1511 17s2 -- The Giving Tree"
        , "# This file was automatically generated."
        , ""
        , "set -e"
        , "tar xf submission.tar"
        , T.concat
            [ "exec /home/cs1511/bin/autotest --no-colorize "
            , activity act
            , " 2>&1 | tee \"!dryrun_record\""
            ]
        ]
      setFileMode dryrun_file 0o755

