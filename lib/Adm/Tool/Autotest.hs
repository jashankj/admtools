{-|
Module:         Adm.Tool.Autotest
Description:    run automated testing for an activity
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

The @autotest@ tool runs an activity's automated tests.

=== Options

 - @-a@, @--autotest@:
   Load an autotest from a particular activity file.

 - @--no-parallel@:
   Don't run tests in parallel;
   by default, @autotest@ will attempt to
   compile and run each test in parallel.

 - @--no-colorize@:
   Don't show output in colour.
   By default, @autotest@ uses ANSI escape codes
   to generate colourful output.
   This option also sets @$DCC_COLORIZE_OUTPUT@
   in the environment.

 - @--no-show-input@:
   Don't show test input on failed tests.

 - @--no-show-expected@:
   Don't show expected outputs on failed tests.

 - @--no-show-actual@:
   Don't show actual outputs on failed tests.

 - @--no-show-diff@:
   Don't show a /diff(1)/ between
   the actual and expected outputs on failed tests.

 - @--no-show-command@:
   Don't show the command needed
   to reproduce a failed test.

 - @--no-fail-stderr@:
   Don't fail tests with correct @stdout@
   and errors on @stderr@;
   it's possible to have gotten the output right,
   and then crash spectacularly.

 - @--no-show-exit-value@:
   Don't show the program's exit value,
   if it was not the expected value.

 - @--keep-artefacts@:
   By default, @autotest@ creates
   a temporary directory to compile and run tests in,
   which is then cleaned up afterwards.
   This option will suppress that clean-up,
   preserving test objects;
   this is primarily of interest when
   debugging autotests.

 - @--version@:
   Show version information.

 - @-h@, @--help@:
   Show online help text.

=== Environment

@autotest@ sanitises its environment before running tests.
The following environment variables are available
in the test environment, either as-is or sanitised:

 * @$CLASS@
 * @$DCC_COLORIZE_OUTPUT@
 * @$DEBUG@
 * @$DRYRUN_COMPILERS@
 * @$DRYRUN_DIR@
 * @$GIVEPERIOD@
 * @$LANG@ (reset to @en_AU.utf8@)
 * @$LANGUAGE@ (reset to @en_AU.UTF-8@)
 * @$LC_ALL@ (reset to @en_AU.UTF-8@)
 * @$LC_COLLATE@ (reset to @POSIX@)
 * @$PATH@
 * @$TESTFILE_DIR@
 * @$TEST_COMPILERS@
 * @$TMPDIR@
 * @$USER@

=== Exit Status
The following exit values may be returned by @autotest@,
which correspond with error messages that are emitted:

[@0@]:  All tests passed; you are awesome!
[@1@]:  One or more tests failed.
[@64@]: The command was used incorrectly.
[@65@]: Test sources were missing.
[@66@]: An unknown activity was specified.
[@74@]: An error occurred while doing I/O.
[@78@]: The activity could not be loaded.

=== History

This version of @autotest@ is
(very) loosely based on a Python script
written by Andrew Taylor <andrewt@cse.unsw.edu.au>.
That script is even more loosely based on
a Perl script of unknown origin,
possibly written by Mei Cheng Whale <meicheng@cse.unsw.edu.au>,
Geoff Whale <geoffw@cse.unsw.edu.au>, or Andrew Taylor.

-}


module Adm.Tool.Autotest (main)
where

import           Control.Applicative ((<|>))
import           Control.Monad (when)
import qualified Control.Monad.Parallel as ParM
import           Data.List (intercalate)
import           Data.Maybe (fromMaybe)
import           Data.Semigroup ((<>))
import           Data.String ()
import qualified Data.Text as T
import           Options.Applicative ( Parser, execParser
                                     , metavar, help, short, long
                                     , argument, option, str, switch )
import           System.Directory ( doesFileExist
                                  , getCurrentDirectory, setCurrentDirectory
                                  , createDirectoryIfMissing
                                  , removeDirectoryRecursive )
import           System.Environment (setEnv)
import           System.FilePath (takeExtension)
import           System.Exit (ExitCode(..), exitWith)

import           Adm.Autotest ( Autotest
                              , descr, f_stdout, f_stderr, exit_code
                              , Options(..)
                              , CompileWith(..), RunWith(..)
                              , TestStatus(..)
                              , TestResult(..)
                              , CompileStatus(..), TestRunStatus(..)
                              )
import           Adm.Autotest.Compile.Cc
import qualified Adm.Autotest.Report as ATReport
import           Adm.Autotest.Run.PlainExecutable
import           Adm.Autotest.Run.Interpreted ()
import qualified Adm.Autotest as Autotest
import           Adm.Activity (Activity, autotest, accept, compile)
import qualified Adm.Activity as Activity
import           Adm.Utils.Errx (Ex(..), errx, warnx)
import           Adm.Utils.Env (Env(..), clean_up_environment)
import           Adm.Utils.Tool (Tool(..), switch', getopt)

------------------------------------------------------------------------

_TOOL :: Tool
_TOOL = Tool
  { tool_name = "autotest"
  , tool_summary = "run automated tests"
  , tool_descr = ""
  , tool_hint = "There aren't any PuzzleQuest clues here..."
  }

getopt' :: Parser Options
getopt' = Options Nothing
  -- Should we run with parallelism?
  <$> (switch'
     ( long  "no-parallel"
    <> help  "don't run tests in parallel"
     ))

  -- Colour-blind students, be warned.  Students who can spell
  -- correctly, my apologies.
  <*> (switch'
     ( long  "no-colorize"
    <> help  "don't show output in glorious colour"
     ))

  -- Some sane defaults: students should know what we did to them,
  -- except if it's not reasonable to do so -- if we're, say, running
  -- an automarking session.

  <*> (switch'
     ( long  "no-show-input"
    <> help  "don't show test input"
     ))
  <*> (switch'
     ( long  "no-show-expected"
    <> help  "don't show expected outputs"
     ))
  <*> (switch'
     ( long  "no-show-actual"
    <> help  "don't show actual outputs"
     ))
  <*> (switch'
     ( long  "no-show-diff"
    <> help  "don't show diff between actual and expected output"
     ))
  <*> (switch'
     ( long  "no-show-command"
    <> help  "don't show command to reproduce test"
     ))

  -- It's possible to have gotten the output right, and then crash
  -- spectacularly.  Catch these students out.
  <*> (switch'
     ( long  "no-fail-stderr"
    <> help ("don't fail tests with correct stdout " ++
             "and errors on stderr")
     ))

  -- Show a program's value, maybe.
  <*> (switch'
     ( long  "no-show-exit-value"
    <> help  "don't show program exit value"
     ))

  -- Keep build artefacts in their own directories.
  <*> (switch
     ( long  "keep-artefacts"
    <> help  "keep temporary build and test artefacts"
     ))

  -- Now, either read an activity name, or an option for where the
  -- activity comes from.
  <*> ( (argument str (metavar "ACTIVITY"))
    <|> (option str (
            ( long  "activity"
           <> short 'a'
           <> help  "load autotests from particular activity file"
           <> metavar ".../ACTIVITY.toml"
            )))
      )

main :: IO ()
main = do
  old_cwd <- getCurrentDirectory
  opts' <- execParser (getopt _TOOL getopt')
  let opts = opts' { Autotest.old_pwd = Just old_cwd }

  -- Tidy up the environment.
  clean_up_environment our_env
  set_dcc_colorized_output opts

  -- Load the activity, and ensure it makes sense to run tests.
  let name_or_path = Autotest.autotest_ opts
  act <-
    if ((takeExtension name_or_path) == ".toml")
    then Activity.load_from_file name_or_path
    else Activity.load name_or_path
  ensure_have_source_files act

  -- Determine, create, and move into, a temporary directory.
  tmpdir <- Autotest.get_activity_tmpdir act
  createDirectoryIfMissing True tmpdir
  setCurrentDirectory tmpdir

  -- Load tests.
  let tests = autotest act

  -- Run the tests!
  results <- run_tests opts act tests

  -- Clean up the temporary directories, if we're told to do so.
  setCurrentDirectory old_cwd
  if Autotest.keep_artefacts opts
  then do warnx $ "preserving temporary directory '"++ tmpdir ++"'"
  else do removeDirectoryRecursive tmpdir

  -- How did the tests go?
  ATReport.tests opts results
  let (n_tests, passed, _, _) =
        Autotest.test_statistics results

  exitWith $
    if n_tests == passed
    then ExitSuccess
    else ExitFailure 1

------------------------------------------------------------------------

our_env :: Env
our_env = Env
  { safe_env_vars =
    [ "CLASS"
    , "DCC_COLORIZE_OUTPUT"
    , "DEBUG"
    , "DRYRUN_COMPILERS"
    , "DRYRUN_DIR"
    , "GIVEPERIOD"
    , "LANG"
    , "LANGUAGE"
    , "LC_ALL"
    , "LC_COLLATE"
    , "PATH"
    , "TESTFILE_DIR"
    , "TEST_COMPILERS"
    , "TMPDIR"
    , "USER"
    ]
  , set_env_vars =
      [ ("LANG", "en_AU.utf8")
      , ("LANGUAGE", "en_AU.UTF-8")
      , ("LC_ALL", "en_AU.UTF-8")
      , ("LC_COLLATE", "POSIX")
  --    , ("TESTFILE_DIR", "args.autotest_directory")
  --    , ("TEST_COMPILERS", "args.c_compilers")
      ]
  }

set_dcc_colorized_output :: Options -> IO ()
set_dcc_colorized_output opts = do
  setEnv "DCC_COLORIZE_OUTPUT"
    ( if Autotest.colorize opts
      then "true"
      else "false"
    )


------------------------------------------------------------------------

-- An activity defines a list of sources that will be compiled.
ensure_have_source_files :: Activity -> IO ()
ensure_have_source_files act = do
  let compile_sources = map T.unpack $ accept act
  sources_present <- mapM doesFileExist $ compile_sources
  when (any (== False) sources_present)
       (errx EX_DATAERR $
         "required source files missing:\n" ++
         (intercalate "\n" $
           map ("    " ++) $
           map fst $
           filter (\x -> snd x == False) $
           zip compile_sources sources_present))
  return ()


------------------------------------------------------------------------

-- Run the test suite; we run in parallel with Control.Monad.Parallel
-- to provide reasonable performance, and sequence results back
-- together at the end.
run_tests
  :: Options
  -> Activity
  -> [Autotest]
  -> IO [TestStatus]
run_tests opts act tests = do
  let compilers' = fromMaybe [] $ compile act
  let compilers  = map name_to_compiler compilers'
  pmapM (run_one_test opts act) $
    numbered $ with_all_compilers compilers tests
  where
    pmapM =
      if Autotest.parallel opts
      then ParM.mapM else mapM
    numbered xs =
      map (\((c, t), n) -> (c, t, n)) $ zip xs [1..]
    with_all_compilers cs ts =
      concat $ map (\t -> map (\c -> (c, t)) cs) ts
    name_to_compiler x =
      case x of
        "dcc" ->
          Cc "dcc" []
        "vcc" ->
          Cc "dcc" ["--valgrind"]
        "dcc --valgrind" ->
          Cc "dcc" ["--valgrind"]
        "dcc --leak-check" ->
          Cc "dcc" ["--leak-check"]
        "gcc" ->
          Cc "gcc" []
        "clang" ->
          Cc "clang" []
        "3c" ->
          Cc "3c" []
        "3c+leak" ->
          Cc "3c" ["+leak"]
        _ -> undefined

-- Run a test.  There's probably a much more haskell-y way to do this,
-- but I'm currently lazy, and refactoring is for the week.
run_one_test
  :: CompileWith cc
  => Show cc
  => Options
  -> Activity
  -> (cc, Autotest, Int)
  -> IO TestStatus
run_one_test opts act (t_compiler, test, n) = do
  let (Just old_pwd') = Autotest.old_pwd opts
  tmpdir <- Autotest.set_up_tmpdir old_pwd' act test
  let bin' = T.unpack $ descr test
  let bin  = concat [bin', "_", show t_compiler]

  compile_ <- do_compile t_compiler tmpdir bin act test

  -- Load up some expected results.
  expected_result <-
    test_load_result act test

  -- Run the test.
  actual_result <-
    case compile_ of
      CompileOk -> do
        run <- do_run (PlainExecutable bin) tmpdir act test
        return $ Just run
      CompileFailed -> do
        return $ Nothing

  -- And give back a test status.
  let ts = TestStatus
           { compiled = compile_
           , executed =
               if compile_ /= CompileOk
               then TestError
               else if expected_result /= actual_result
                    then TestFailed
                    else TestPassed
           , expected = expected_result
           , actual   = actual_result
           }

  -- Post-test report for this test.
  ATReport.fallout opts act test ts
  ATReport.one_test n test ts
  return ts


------------------------------------------------------------------------

{-
-- | The magic "no test ran" value.
no_test :: TestResult
no_test =
  TestResult B.empty B.empty (ExitFailure (-1))
-}

------------------------------------------------------------------------

-- | Fixture loading and checking:
test_load_result :: Activity -> Autotest -> IO (Maybe TestResult)
test_load_result act test = do
  stdout_bytes <- Autotest.load_bytes act $ f_stdout test
  stderr_bytes <- Autotest.load_bytes act $ f_stderr test
  let exit = case exit_code test of
        Just 0  -> ExitSuccess
        Just t  -> ExitFailure t
        Nothing -> ExitSuccess
  return $ Just $ TestResult stdout_bytes stderr_bytes exit


------------------------------------------------------------------------
