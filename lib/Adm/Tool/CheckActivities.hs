{-|
Module:         Adm.Tool.CheckActivities
Description:    load and test activities
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

@check-activities@ attempts to load all ACTIVITY.toml files
specified, and reports if they could not be loaded.  This is
to ensure that all code in an ACTIVITY.toml is always valid,
and can be used from a continuous integration tool.

=== Options

 - @--version@:
   Show version information.

 - @-h@, @--help@:
   Show online help text.

 - @ACTIVITY.toml...@:
   A list of @ACTIVITY.toml@ files.

-}

module Adm.Tool.CheckActivities (main)
where

import           Control.Applicative (some)
import           Data.List ()
import           Data.Semigroup ((<>))
import           Options.Applicative ( Parser
                                     , metavar, argument, str
                                     , help, execParser )
import           System.FilePath ((</>))

import           Adm.Autotest ()
import qualified Adm.Activity as Activity
import           Adm.Utils.Tool (Tool(..), getopt)

------------------------------------------------------------------------

_TOOL :: Tool
_TOOL = Tool
  { tool_name = "check-activities"
  , tool_summary = "ACTIVITY.toml sanity checker"
  , tool_descr = "Linter for ACTIVITY.toml syntax."
  , tool_hint = "There is no PuzzleQuest in Ba Sing Se."
  }

data Options = Options
  { files :: [String]
  }

getopt' :: Parser Options
getopt' = Options
  -- A list of activities to load.
  <$> (some $ argument str
     ( metavar "ACTIVITY-DIR..."
    <> help ( "A list of directories that contain an "
           ++ "ACTIVITY.toml and associated fixtures.")
     ))

------------------------------------------------------------------------

main :: IO ()
main = do
  opts <- execParser (getopt _TOOL getopt')
  mapM_
    (\x -> do
        a <- Activity.load_one (x </> "ACTIVITY.toml")
        return $ (x, a)) $ files opts
