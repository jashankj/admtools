{-|
Module:         Adm.Tool.ActivityToWeb
Description:    convert an activity to a webpage
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

The @activity2web@ tool converts an @ACTIVITY.toml@ file
and associated fixtures to a web page.

=== Options

 - @--no-parallel@:
   Don't generate in parallel;
   by default, @activity2web@ will attempt to
   generate activity files in parallel.

 - @--version@:
   Show version information.

 - @-h@, @--help@:
   Show online help text.

 - @ACTIVITY.toml...@:
   A list of @ACTIVITY.toml@ files.

=== Environment

@activity2web@ uses the current working directory.

=== Examples

To render the activities locally, a user might like to do:

@
$ cd \/home\/cs1511\/17s2_materials
$ activity2web _activities\/*
$ jekyll build
@

-}
------------------------------------------------------------------------


module Adm.Tool.ActivityToWeb (main)
where

import           Control.Applicative (some)
import qualified Control.Monad.Parallel as ParM
import           Data.Maybe (fromMaybe, isJust)
import           Data.Semigroup ((<>))
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import           Options.Applicative ( Parser
                                     , metavar, argument, str
                                     , help, long, execParser )
import           System.Directory (copyFile, createDirectoryIfMissing)
import           System.FilePath ((</>), takeDirectory)

import           Adm.Autotest ()
import           Adm.Activity (Activity, provide)
import qualified Adm.Activity as Activity
import           Adm.Utils.Errx
import           Adm.Utils.Tool (Tool(..), switch', getopt)


------------------------------------------------------------------------

_TOOL :: Tool
_TOOL = Tool
  { tool_name = "activity2web"
  , tool_summary = "ACTIVITY.toml to activity web pages"
  , tool_descr = "Converts a set of activity descriptions "
              ++ "in TOML format, and associated fixtures "
              ++ "including stubs, source, and tests, "
              ++ "into Markdown and files in a directory hierachy "
              ++ "appropriate for a Jekyll site build."
  , tool_hint = "PuzzleQuest?  What PuzzleQuest?"
  }

data Options = Options
  { parallel :: Bool
  , files :: [String]
  }

getopt' :: Parser Options
getopt' = Options
  -- Should we run with parallelism?
  <$> (switch'
     ( long  "no-parallel"
    <> help  "don't generate files in parallel"
     ))

  -- A list of activities to load.
  <*> (some $ argument str
     ( metavar "ACTIVITY-DIR..."
    <> help ( "A list of directories that contain an "
           ++ "ACTIVITY.toml and associated fixtures.")
     ))

------------------------------------------------------------------------

-- | Load the activity, and make web files for these activities.
main :: IO ()
main = do
  opts <- execParser (getopt _TOOL getopt')

  activities <-
    mapM (\x -> do
             a <- Activity.load_one (x </> "ACTIVITY.toml")
             return $ (a, x)) $ files opts

  make_web_files opts activities

------------------------------------------------------------------------

-- | Take a list of activities and turn them all into files for the web.
make_web_files :: Options -> [(Maybe Activity, FilePath)] -> IO ()
make_web_files opts acts =
  pmapM_ make_web_files' $ filter just_fsts $ acts
  where
    pmapM_ = if parallel opts then ParM.mapM_ else mapM_
    make_web_files' (Just act, dir) = do
      make_web_file act
      provide_files act dir
    make_web_files' (_, dir) = do
      warnx $ "Couldn't load 'ACTIVITY.toml' from '" ++ dir ++ "'"
    just_fsts = isJust . fst

-- | Take one activity and turn it into a file for the web.
make_web_file :: Activity -> IO ()
make_web_file act = do
  putStrLn $ "\t[GEN] '" ++ file ++ "'"
  createDirectoryIfMissing True $ takeDirectory file
  TIO.writeFile file body
  where
    file = Activity.web_file act
    body = Activity.webify act

-- | Take one activity and turn it into a file for the web.
provide_files :: Activity -> FilePath -> IO ()
provide_files act src_dir = do
  let
    dest_dir = Activity.web_dir act </> "files"
    act_files = map T.unpack $ fromMaybe [] $ provide act
    copy_one f =
      let from_ = (src_dir </>)
          to_   = (dest_dir </>)
      in do
        putStrLn $ "\t[CP] '" ++ (from_ f) ++ "' -> '" ++ (to_ f) ++ "'"
        copyFile (from_ f) (to_ f)
  createDirectoryIfMissing True dest_dir
  mapM_ copy_one $ act_files


------------------------------------------------------------------------
