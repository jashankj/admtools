{-|
Module:         Adm.Tool.MkWeb
Description:    regenerate webpages
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

`mkweb` rebuilds a course website, using Hakyll to manage the
actual site generation logic.
-}


module Adm.Tool.MkWeb (main)
where

import qualified Adm.Web.Site as Site
import           Hakyll
import           System.Exit
import           Adm.Utils.Errx
import           Adm.Utils.Tool (Tool(..))

------------------------------------------------------------------------

_TOOL :: Tool
_TOOL = Tool
  { tool_name = "mkweb"
  , tool_summary = "regenerate course website"
  , tool_descr = ""
  , tool_hint = "The only PuzzleQuest in the village."
  }

main :: IO ()
main = do
  config   <- Site.config
  let opts  = Options False Build
  rules    <- Site.site
  e <- hakyllWithExitCodeAndArgs config opts rules
  finish e

  where
    finish (ExitSuccess)    = return ()
    finish (ExitFailure e') =
      (case retval_to_ex e' of
         (Just e'') -> errx e''
         Nothing    -> errx EXIT_FAILURE
      ) "couldn't build site"
