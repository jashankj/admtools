{-|
Module:         Adm.Activity
Description:    an activity
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

An Activity is a way to describe the group of related content
needed to manage one of the "activities" that form a part of
each module's lab work.  From an Activity, we can generate a
Give spec, pages, autotest data, etc. etc.

An Activity has the following structure:

_activities/
└╴ activity-name/
   ├╴ ACTIVITY.toml
   ├╴ [resources to provide]
   └╴ [resources for autotesting]

The `ACTIVITY.toml` file contains several mandatory fields,
and several optional fields.  The mandatory fields are:

 * `activity`, the machine name of the activity;

 * `name`, the human-readable name of the activity;

 * `in_week`, the week this activity belongs, and
 * `order`, the sequence in that week's activities;

 * `brief`, the "brief" of the activity, describing the task
   to be completed by students, and relevant instructions
   about how to do it well;

 * `accept`, a list of files to accept as student submissions;

The optional fields are:

 * `provide`, a list of files to provide to students;

 * `compile`, a list of compilers to use

 * `deadline`, which has one mandatory field and two optional
   fields:

   * `due`, when the submission is due; and, optionally,
   * `open` and `end`, when the submission opens and closes;

 * `solution`, which gives a solution fixture;

 * `give`, which has several optional fields:
   * `late-penalty`, a list of late penalty rules;
   * `extensions`, a table of student-to-extension mappings;
   * `compile`, a block of text of compilation directives;
   * `subjective`, a table giving a subjective marking scheme;
   * `special`, a special-mark field.

 * `autotest`, which lists a set of automated tests; these are
   described in the Autotest module documentation.

-}
------------------------------------------------------------------------


module Adm.Activity
  ( -- * the 'Activity' type
    ActivityType(..)
  , Activity(..)

    -- * load activities
  , load_all
  , load_one
  , load, load_from_file

  , webify
  , give_name
  , web_dir
  , web_file
  )
where

import           Control.Monad
import           Data.Aeson
import           Data.Maybe
import           Data.Text (Text)
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import           GHC.Generics
import           System.Directory hiding (listDirectory)
import           System.FilePath
import           System.IO (IOMode(..), withFile)
import           Text.Printf

import           {-# SOURCE #-} Adm.Autotest
import           Adm.Give
import           Adm.Utils.Errx
import qualified Adm.Utils.Paths as Paths
import qualified Adm.Utils.Toml as Toml

------------------------------------------------------------------------

data Activity = Activity
  { act_directory :: Maybe FilePath
  , activity :: Text
  , activity_type :: ActivityType
  , act_type :: Maybe Text
  , act_when :: Maybe Text
  , name :: Text
  , brief :: Text
  , accept :: [Text]
  , provide :: Maybe [Text]
  , compile :: Maybe [Text]
  , deadline :: Maybe DeadlineSpec
  , give_options :: Maybe GiveOptions
  , autotest :: [Autotest]
  } deriving (Show, Eq, Generic)

data ActivityType
  = InWeek (Integer, Integer)
  | Assignment (Integer)
  | Freeform
  deriving (Show, Eq, Generic)

instance FromJSON Activity where
  parseJSON = withObject "Activity" $
    \v -> activity_with_maybe_autotest
    <$> v .:  "activity"
    <*> v .:? "in_week"
    <*> v .:? "order"
    <*> v .:? "assignment"
    <*> v .:? "type"
    <*> v .:? "when"
    <*> v .:  "name"
    <*> v .:  "brief"
    <*> v .:  "accept"
    <*> v .:? "provide"
    <*> v .:? "compile"
    <*> v .:? "deadline"
    <*> v .:? "give"
    <*> v .:? "autotest"
    where
    activity_with_maybe_autotest
      _1 _2 _3 _4 _5 _6 _7 _8 _9 _10 _11 _12 _13 _14 =
      Activity Nothing
        _1 (activity_type_from _2 _3 _4)
        _5 _6 _7 _8 _9 _10 _11 _12 _13 $ fromMaybe [] _14
    activity_type_from iw iwo a =
      case (iw, iwo, a) of
        (Just iw_, Just o_, Nothing) ->
          InWeek (iw_, o_)
        (Nothing, Nothing, Just a_) ->
          Assignment (a_)
        (Nothing, Nothing, Nothing) ->
          Freeform
        (_, _, _) ->
          error $ "both 'in_week'/'order' and 'assignment' in Activity"

------------------------------------------------------------------------

load_all :: IO [Activity]
load_all = fmap catMaybes $ load'
  where
    load' = do
      act_dir   <- Paths.adm_activities
      act_dirs' <- listDirectory act_dir
      let act_dirs = map (act_dir </>) $ act_dirs'
      mapM load_one $ map (</> "ACTIVITY.toml") $ act_dirs

-- | `directory` doesn't get `listDirectory` until 1.2.5; improvise.
-- I don't care if we shadow the outer one.
listDirectory :: FilePath -> IO [FilePath]
listDirectory path =
  (filter f) <$> (getDirectoryContents path)
  where f filename = filename /= "." && filename /= ".."

load_one :: FilePath -> IO (Maybe Activity)
load_one activity_file = do
  str <- withFile (activity_file) ReadMode TIO.hGetContents
  return $ Toml.maybe_parse activity_file str


------------------------------------------------------------------------

-- | Otherwise, we _have_ an activity; load it.  We can `fromJust`
-- here because we dispose of the program given the Nothing case.
load :: FilePath -> IO Activity
load at_name' = do
  act_dir <- fmap (</> at_name') Paths.adm_activities
  load_from_file $ act_dir </> "ACTIVITY.toml"

load_from_file :: FilePath -> IO Activity
load_from_file activity_file = do
  act <- load_one $ activity_file
  when (isNothing act)
       (errx EX_CONFIG $ "couldn't load '" ++ activity_file ++ "'")
  let (Just act') = act
  file' <- canonicalizePath activity_file
  let act_dir = takeDirectory file'
  return $ act' { act_directory = (Just act_dir) }


------------------------------------------------------------------------

-- | Get an activity's give name, like `wk01_bird`, given an activity
-- in week 1 called `bird`.
give_name :: Activity -> String
give_name act =
  case activity_type act of
    InWeek (w, _) ->
      printf "wk%02d_%s" w (activity act)
    Assignment n ->
      printf "assign%01d" n
    Freeform ->
      T.unpack $ activity act


------------------------------------------------------------------------

web_dir :: Activity -> FilePath
web_dir act =
  case activity act of
    "testGame" -> "assignments/assign2"
    "Game"     -> "assignments/assign2"
    "player"   -> "assignments/assign2"
    _ -> web_dir' act
web_dir' :: Activity -> FilePath
web_dir' act =
  case activity_type act of
    InWeek (w, _) ->
      printf "week%02d" w
    Assignment n ->
      printf "assignments/assign%01d" n
    Freeform ->
      T.unpack $ activity act

web_file :: Activity -> FilePath
web_file act =
  case activity act of
    "testGame" -> "assignments/assign2/testGame.markdown"
    "Game"     -> "assignments/assign2/Game.markdown"
    "player"   -> "assignments/assign2/player.markdown"
    _ -> web_file' act
web_file' :: Activity -> FilePath
web_file' act =
  case activity_type act of
    InWeek (w, o) ->
      printf "week%02d/%02d_%s.markdown" w o (activity act)
    Assignment n ->
      printf "assignments/assign%01d.markdown" n
    Freeform ->
      printf "%s.markdown" (activity act)

-- | Take one activity and turn it into text for a file for the web.
webify :: Activity -> Text
webify act =
  T.unlines $
    [ "---"
    , frontmatter act
    , "---"
    , ""
    , "{::options parse_block_html=\"true\" /}"
    , ""
    , brief act
    , ""
    , if not . null . autotest $ act
      then "{% include autotest.html %}"
      else ""
    , "{% include stylomatic.html %}"
    , "{% include submit.html %}"
    ]
  where

    -- Take an activity, and generate its YAML frontmatter; Act→Text
    frontmatter act_ = T.intercalate "\n" $ map yaml $
      [ ("title", dquo $ name act_)
      , ("give", give_fm act_)
      , ("activity", activity_fm act_)
      ]

    -- Activity info, out of the activity and into YAML.
    activity_fm act_ =
      T.append "\n" .
      T.intercalate "\n" .
      map (T.append "  ") .
      map yaml $
      [ ("name", dquo $ activity act_)
      , ("type", dquo $ fromMaybe T.empty $ act_type act_)
      , ("when", dquo $ fromMaybe T.empty $ act_when act_)
      ]

    -- Take Give-specific information out of the activity and into YAML.
    give_fm act_ =
      T.append "\n" .
      T.intercalate "\n" .
      map (T.append "  ") .
      map yaml $
      [ ("assign", dquo . T.pack . give_name $ act_)
      , ("accept", arr $ map dquo $ accept act_)
      ]

    -- Take a tuple, and make it smell like YAML; (Text,Text) → Text
    yaml (k, v) =
      T.append k $ T.append ": " v

    -- Bracket and join a list; [Text] → Text.
    arr a = T.append "[" $ T.append (T.intercalate "," a) "]"

    -- Quote a string; Text → Text.
    dquo a = T.append "\"" $ T.append a "\""

------------------------------------------------------------------------
