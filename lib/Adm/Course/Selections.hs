{-|
Module:         Adm.Course.Selections
Description:    course "selections" for teaching staff
Copyright:      Jashank Jeremy, 2018
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

-}
------------------------------------------------------------------------


module Adm.Course.Selections
  ( )
where
