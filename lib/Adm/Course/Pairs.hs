{-|
Module:         Adm.Course.Pairs
Description:    manage pairs of students
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

-}
------------------------------------------------------------------------


module Adm.Course.Pairs
  ( Pair, PairK, PairV, PairMap
  , PairSet, pairset
  , PairEntry, pair, students

  , load_pairs_list
  )
where

import           Control.Monad
import           Data.Aeson
import           Data.Map (Map)
import qualified Data.Map as Map
import           Data.Maybe
import           Data.Text (Text)
import qualified Data.Text.IO as TIO
import           System.IO

import           Adm.Give
import           Adm.Utils.Errx
import qualified Adm.Utils.Toml as Toml

------------------------------------------------------------------------

type PairK = Text
type PairV = [User]
type Pair = (PairK, PairV)
type PairMap = Map PairK PairV

data PairEntry = PairEntry
  { pair :: Text
  , students :: [User]
  }
  deriving (Show, Eq, Ord)

instance FromJSON PairEntry where
  parseJSON = withObject "Pair" $
    \v -> PairEntry
    <$> v .:  "pair"
    <*> v .:  "students"

data PairSet = PairSet { pairset :: [PairEntry] }

instance FromJSON PairSet where
  parseJSON = withObject "PairSet" $
    \v -> PairSet
    <$> v .:  "pair"


------------------------------------------------------------------------

load_pairs_list :: FilePath -> IO PairMap
load_pairs_list fp =
  fmap add_pairs $ load_pairs fp
  where
    add_pairs = foldr (insert_pair) (Map.empty)
    insert_pair p = Map.insert (pair p) (students p)

load_pairs :: FilePath -> IO [PairEntry]
load_pairs fp = do
  pairs_str <- withFile fp ReadMode TIO.hGetContents
  let maybe_pairs = Toml.maybe_parse fp pairs_str
  when (isNothing maybe_pairs)
       (errx EX_DATAERR ("couldn't load '" ++ fp ++ "'"))
  let (Just pairs_) = maybe_pairs
  return $ pairset pairs_


------------------------------------------------------------------------
