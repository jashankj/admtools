{-|
Module:         Adm.Course.Exam
Description:    describe seating for an examination
Copyright:      Jashank Jeremy, 2018
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

-}
------------------------------------------------------------------------


module Adm.Course.Exam
  ( )
where

import qualified Data.Text as T
import           Data.Text (Text)
import           Data.Time

data Exam = Exam
  { sessions :: [Session] }

data Session = Session
  { location   :: Text
  , start_time :: UTCTime
  , end_time   :: UTCTime
  }

deriving instance Show Exam
deriving instance Show Session
