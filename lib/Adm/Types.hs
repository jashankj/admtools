{-|
Module:         Adm.Types
Description:    types used throughout admtools
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      deprecated
Portability:    POSIX

XXX this library needs to be dismantled!

-}
------------------------------------------------------------------------

module Adm.Types
  ( User
  , UPI
  )
where

import           Data.Text (Text)

------------------------------------------------------------------------

-- | Usernames.
type User = Text
-- | Student IDs, or "uniform person identifiers".
type UPI = Int

