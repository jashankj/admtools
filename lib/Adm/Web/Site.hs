{-|
Module:         Adm.Web.Site
Description:    generate a course website
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

Use Hakyll to generate a course website.

-}


module Adm.Web.Site
  ( site, config )
where

import           Data.Functor
import           Data.List (isInfixOf, isSuffixOf)
import qualified Data.Set as S
import qualified Data.Text as T
import           Hakyll
import           Hakyll.Web.Hamlet
import           Hakyll.Web.Sass
import           System.FilePath ( (</>)
                                 , takeBaseName, takeDirectory, splitFileName)
import           Text.Pandoc.Options

import           Adm.Web.Layout.Default (layout_default)
import qualified Adm.Web.BlazeTmpl as Tmpl
import qualified Adm.Utils.Paths as Paths
import qualified Adm.Course as Course

-- | Generate a site configuration.
config :: IO Hakyll.Configuration
config = do
  site_dir <- Paths.adm <&> (</> ".src")
  return $
    defaultConfiguration
      { providerDirectory    = site_dir
      , destinationDirectory = site_dir </> "_site"
      }

course_config :: IO Course.Configuration
course_config = Course.load

site :: IO (Rules ())
site = course_config >>= \cfg ->
  return $ do
    match "assets/*.js"   $ route idRoute >> compile copyFileCompiler
    match "assets/*.css"  $ route idRoute >> compile copyFileCompiler
    match "assets/*.scss" $ do
      let sassOpts = def { sassIncludePaths = pure [ "_sass", "_vendor" ] }
      route   $ setExtension "css"
      compile $ sassCompilerWith sassOpts

    match "index.md" $ do
      route   $ setExtension "html"
      compile $ pandocCompiler
        >>= Tmpl.applyTemplate layout_default (cONTEXT cfg)
        >>= relativizeUrls

cONTEXT :: Course.Configuration -> Context String
cONTEXT cfg = mconcat
  [ pageContext, courseContext, siteContext
  , bodyField "content", missingField ]
  where
    pageContext = mconcat
      [ titleField "page.title"
      , urlField   "page.url"
      , pathField  "page.path"
      ]
    courseContext = mconcat
      [ constField "course.account" (T.unpack $ Course.course_account course)
      , constField "course.code"    (T.unpack $ Course.course_code    course)
      , constField "course.email"   (T.unpack $ Course.course_email   course)
      , constField "course.name"    (T.unpack $ Course.course_name    course)
      , constField "course.session" (T.unpack $ Course.course_session course)
      ]
    siteContext
      = mempty
    course = Course.course cfg

------------------------------------------------------------------------
-- derived from http://yannesposito.com/Scratch/en/blog/Hakyll-setup/

-- replace a foo/bar.md by foo/bar/index.html
-- this way the url looks like: foo/bar in most browsers
cleanRoute :: Routes
cleanRoute = customRoute createIndexRoute
  where createIndexRoute ident
          = takeDirectory p </> takeBaseName p </> "index.html"
          where p = toFilePath ident

-- replace url of the form foo/bar/index.html by foo/bar
removeIndexHtml :: Item String -> Compiler (Item String)
removeIndexHtml item = return $ fmap (withUrls removeIndexStr) item
  where
    removeIndexStr :: String -> String
    removeIndexStr url = case splitFileName url of
        (dir, "index.html") | isLocal dir -> dir
        _                                 -> url
        where isLocal uri = not (isInfixOf "://" uri)

