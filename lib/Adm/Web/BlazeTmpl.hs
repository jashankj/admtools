{-|
Module:         Adm.Web.BlazeTmpl
Description:    Blaze templates for Hakyll
License:        BSD3
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental

@Hakyll.Web.Template.Blaze@ was deprecated upstream!
-}

module Adm.Web.BlazeTmpl
  ( Template, applyTemplate
  , ctx_str {-, ctx_list -} )
where

import           Hakyll.Core.Compiler
import           Hakyll.Core.Item
import           Hakyll.Web.Template.Context
import           Text.Blaze.Html                 ( Html )
import           Text.Blaze.Html.Renderer.String ( renderHtml )

type Template m a = (String -> m ContextField) -> Item a -> m Html

applyTemplate :: Template Compiler a    -- ^ Template
              -> Context a              -- ^ Context
              -> Item a                 -- ^ Page
              -> Compiler (Item String) -- ^ Resulting item
applyTemplate tmpl ctx item =
  tmpl ctx' item
  >>= return . renderHtml
  >>= \body -> return $ itemSetBody body item
  where ctx' key = unContext (ctx <> missingField) key [] item


ctx_str ctx key = ctx key
  >>= \case
       (StringField s) -> return s
       (ListField _ _) -> fail $
         "Adm.Web.BlazeTmpl.ctx_str: " ++
         key ++ ": expected StringField, got ListField"

-- type variable leaks!
-- ctx_list ctx key = ctx key
--   >>= \case
--        (ListField _ xs) -> return xs
--        (StringField s)  -> fail $
--          "Adm.Web.BlazeTmpl.ctx_list: " ++
--          key ++ ": expected ListField, got StringField"
