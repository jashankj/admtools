{-|
Module:         Adm.Web.Layout.Util
Description:    course website utility functions.
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX
-}


module Adm.Web.Layout.Util
  (classes)
where

import           Data.Text (Text)
import qualified Data.Text as T
import           Prelude hiding (head, id, div)
import           Text.Blaze
import qualified Text.Blaze.XHtml5.Attributes as A

-- | A @class@ attribute, made up of a list of CSS classes to apply;
--   useful for (e.g.,) Bootstrap, which uses a huge range of classes
--   applied to a range of elements.
--
--   @
--   X.button ! classes ["btn", "btn-primary", "text-center"]
--   @
classes :: [Text] -> Attribute
classes cs = A.class_ $ textValue (T.unwords cs)
