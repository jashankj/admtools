{-|
Module:         Adm.Web.Layout.Default
Description:    course website page layout
Copyright:      Jashank Jeremy, 2017-18
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

These templates form the "layout" of webpages on a course website.

They're a pretty close port of Liquid templates built for COMP1511 17s2,
with some additional tweaks discovered since (e.g., flexboxing to push
the page footer down to the bottom of the page in a nice way).

-}


module Adm.Web.Layout.Default
  ( layout_default )
where

import           Data.List (isInfixOf, isSuffixOf)
import           Data.Text (Text)
import qualified Data.Text as T
import           Prelude hiding (head, id, div, span)
import           System.FilePath

import           Text.Blaze
import           Text.Blaze.XHtml5
import qualified Text.Blaze.XHtml5.Attributes as A

import           Hakyll (Context(..), ContextField(..), Compiler, itemBody)
import           Adm.Web.BlazeTmpl
import           Adm.Web.Layout.Util (classes)

_CSE_WEBSITE :: String
_CSE_WEBSITE = "https://www.cse.unsw.edu.au"

-- | The default page layout for pages on the course website.
layout_default :: Template Compiler String
layout_default ctx item = do
  let ctx_str' = ctx_str ctx

  course_account <- ctx_str' "course.account"
  course_code    <- ctx_str' "course.code"
  course_email   <- ctx_str' "course.email"
  course_name    <- ctx_str' "course.name"
  course_session <- ctx_str' "course.session"
  page_title     <- ctx_str' "page.title"
  page_url       <- ctx_str' "page.url"

  navbar' <- navbar ctx item
  breadcrumbs' <- breadcrumbs ctx item

  return $ do
    docType
    html ! A.xmlns "http://www.w3.org/1999/xhtml"
         ! A.lang "en" $ do
      head $ do
        meta ! A.charset "utf-8"
        meta ! A.httpEquiv "X-UA-Compatible"
             ! A.content "IE=edge"
        meta ! A.name "viewport"
             ! A.content "width=device-width, initial-scale=1, shrink-to-fit=no"

        title $ string page_title

        link ! A.rel "stylesheet"
             ! A.href "/assets/main.css"
        link ! A.rel "canonical"
             ! A.href (preEscapedStringValue $ concat
                       [ _CSE_WEBSITE
                       , "/~", course_account
                       , "/", course_session
                       , {- "/", -} page_url ])

      body ! classes ["d-flex", "flex-column"]
           ! A.style "min-height: 100vh;" $ do

        -- layout_header:
        navbar'
        container ! A.id "breadcrumb"
                  $ breadcrumbs'

        main ! classes ["container"]
             ! A.style "flex: 1;" $ do
          preEscapedString $ itemBody item

        footer ! classes ["mt-3", "py-3", "text-center", "no-print", "bg-dark"] $ do
          p ! classes ["text-muted"] $ do
            strong $ string $ unwords [course_code, course_session ++ ":", course_name]
            text " is brought to you by" >> br

            text "the "
            a ! A.href (preEscapedStringValue _CSE_WEBSITE)
              $ text "School of Computer Science and Engineering"

            text " at the "
            a ! A.href "https://www.unsw.edu.au/"
              $ text "University of New South Wales"
            text ", Sydney." >> br

            text "For all enquiries, please email the class account at "
            a ! A.href (preEscapedStringValue $ "mailto:" ++ course_email)
              $ string course_email
            br

            small $ text "CRICOS Provider 00098G"

        mapM_ (script_) scripts

  where
    script_ (_u, _v, _i) =
      script
        ! A.type_ "text/javascript"
        ! A.src (textValue (_u _v))
        ! A.async "1"
        ! integrity (textValue _i)
        ! crossorigin "anonymous"
        $ text ""
    integrity = customAttribute "integrity"
    crossorigin = customAttribute "crossorigin"


-- | A table of JavaScript used by the website.
--   This is used to generate appropriate @<script>@ tags.
scripts :: [ (Text -> Text, Text, Text) ]
scripts =
  [ ( \v -> T.concat ["https://code.jquery.com/jquery-", v, ".slim.min.js"]
    , "3.1.1"
    , "sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n")
  , ( \v -> T.concat [ "https://cdnjs.cloudflare.com/ajax/libs/tether/"
                     , v, "/js/tether.min.js" ]
    , "1.4.0"
    , "sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb")
  , ( \v -> T.concat [ "https://stackpath.bootstrapcdn.com/bootstrap/"
                     , v, "/js/bootstrap.min.js" ]
    , "4.1.1"
    , "sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T")
  , ( \v -> T.concat [ "https://cdnjs.cloudflare.com/ajax/libs/mathjax/"
                     , v, "/MathJax.js?config=TeX-MML-AM_CHTML" ]
    , "2.7.4"
    , "sha256-GhM+5JHb6QUzOQPXSJLEWP7R73CbkisjzK5Eyij4U9w=" )
  ]


------------------------------------------------------------------------

-- | A Bootstrap navigation bar.
--
--   See <https://getbootstrap.com/docs/4.1/components/navbar/>
--   for a description of Bootstrap's navbar component.
--
--   TODO: pass a Hakyll 'Context' down here, to generate navbar links
--   and relative/absolute URLs.
navbar :: Template Compiler String
navbar ctx _item = do
  course_code <- ctx_str ctx "course.code"
  course_session <- ctx_str ctx "course.session"

  return $ do
    nav ! classes [ "navbar", "fixed-top", "navbar-expand-lg"
                  , "navbar-dark", "bg-primary", "no-print" ]
        ! A.id "header-navbar"
        $ do
      button ! classes [ "navbar-toggler", "navbar-toggler-right" ]
             ! A.type_ "button"
             ! customAttribute "data-toggle" "collapse"
             ! customAttribute "data-target" "#navmenu"
             ! customAttribute "aria-controls" "navmenu"
             ! customAttribute "aria-label" "Toggle navigation"
             ! customAttribute "aria-expanded" "false"
             $ do
        span ! classes [ "navbar-toggler-icon" ]
             $ text ""

      container $ do
        a ! classes [ "navbar-brand" ]
          ! A.href "/" {- XXX "/" | relative_url -}
          $ string $ course_code ++ " " ++ course_session
        div ! classes [ "collapse", "navbar-collapse" ]
            ! A.id "navmenu"
            $ do
          ul ! classes [ "navbar-nav", "mr-auto" ]
             $ mapM_ (uncurry (item)) []
    where
      item :: Text -> Text -> Html
      item h t =
        li ! classes [ "nav-item" ]
           $ a ! classes ["navbar-link"]
               ! A.href (textValue h)
               $ text t
        {- include navbar-links.html -}

-- | A list of Bootstrap breadcrumbs.
--
--   See <https://getbootstrap.com/docs/4.1/components/breadcrumb/>
--   for a description of Bootstrap's breadcrumb component.
breadcrumbs :: Template Compiler String
breadcrumbs _ctx _it = return $ do
  ol ! classes [ "breadcrumb", "no-print", "m-0" ]
     $ mapM_ (uncurry (item)) []
  where
    item :: Text -> Text -> Html
    item h t =
      li ! classes [ "breadcrumb-item" ]
         $ a ! A.href (textValue h)
             $ text t

-- | A Bootstrap @div.container@.
container :: Html -> Html
container = div ! classes [ "container" ]

------------------------------------------------------------------------
