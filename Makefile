# Keep this in sync with Adm.cabal.
ADMV		= 0.1.6.0
# Keep this in sync with stack.yaml
RESOLVER	= lts-9.21

.PHONY: all clean
all: adm.cabal
	@stack build
clean:
	@stack clean

adm.cabal: package.yaml
	hpack

cabal.config:
	curl -L "https://www.stackage.org/${RESOLVER}/cabal.config" > ./cabal.config

.PHONY: deploy.cs1511.17s2
deploy.cs1511.17s2:
	@(set -x && \
	hub clone --reference /home/cs1511/17s2_materials COMP1511UNSW/17s2_materials && \
	cd 17s2_materials && \
	git checkout -b adm-${ADMV} && \
	rm -r _tools && mkdir -p _tools && \
	cp -rp ../package.yaml _tools/ && \
	cp -rp ../adm.cabal _tools/ && \
	cp -rp ../cabal.config _tools/ && \
	cp -rp ../stack.yaml _tools/ && \
	cp -rp ../bin _tools/ && \
	cp -rp ../lib _tools/ && \
	git add _tools/ && \
	git commit && \
	git push -u origin adm-${ADMV} && \
	hub pull-request && \
	cd ../ && \
	rm -fr 17s2_materials && \
	true)
