module Main
  (main)
where

import Control.Monad
import System.Directory
import System.Posix.Files
import System.FilePath
import Test.DocTest

main :: IO ()
main = do
  wd <- getCurrentDirectory
  mods <- find_module_files (wd </> "lib")
  doctest $ ["-ilib"] ++ map ("lib" </>) mods

find_module_files :: FilePath -> IO [FilePath]
find_module_files dir = do
  objs <- fmap (map indir) $ listDirectory dir
  let mods = filter (\x -> takeExtension x == ".hs") objs
  dirs <- filterM (isDir) objs
  dirmods <- mapM (find_module_files) dirs
  return . concat $ [mods] ++ dirmods
  where
    isDir x = fmap (isDirectory) $ getFileStatus x
    indir = (dir </>)

