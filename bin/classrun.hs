{-|
Module:         classrun
Description:    gateway to class account facilities
Copyright:      Jashank Jeremy, 2018
License:        BSD-3-Clause
Maintainer:     Jashank Jeremy <jashankj@cse.unsw.edu.au>
Stability:      experimental
Portability:    POSIX

`classrun`

-}
------------------------------------------------------------------------

{-# LANGUAGE ApplicativeDo #-}
{-# LANGUAGE RecordWildCards #-}

module Main where

import           Control.Applicative
import           Control.Monad
import           Data.List
import           Data.Semigroup ((<>))
import           Data.Version (Version(..), showVersion)
import           Options.Applicative

--import           Paths_adm (version)

version :: Version
version = Version [0,1,5,4] []


------------------------------------------------------------------------

data Action
  = Give    { assign :: String
            , files :: [FilePath] }
  | Check   { assign :: String }
  | Collect { assign :: String }
  | Assigns
  | Sturec
  | Mark
  | RSMS
  | Replace { assign :: String
            , group :: String
            , student :: String
            , tarfile :: String
            , backup :: Bool }
  | Remark  { assign :: String
            , group :: String
            , student :: String }
  deriving (Show)

main :: IO ()
main = run =<< execParser getopt

run :: Action -> IO ()
run (Give assign files)
  = undefined
run (Check assign)
  = undefined
run (Collect assign)
  = undefined
run Assigns
  = undefined
run Sturec
  = undefined
run Mark
  = require_authorised
  >> undefined
run RSMS
  = require_authorised
  >> undefined
run (Replace assign group student tarfile backup)
  = require_authorised
  >> undefined
run (Remark assign group student)
  = require_authorised
  >> undefined

require_authorised :: IO ()
require_authorised =
  unless isAuthorised $
  fail "You are not authorised to run this command."

isAuthorised :: Bool
isAuthorised = False

------------------------------------------------------------------------

parse_action :: Parser Action
parse_action
   = hsubparser
   $ parse_give_action
  <> parse_check_action
  <> parse_collect_action
  <> parse_assigns_action
  <> parse_sturec_action
  <> parse_mark_action
  <> parse_rsms_action
  <> parse_replace_action
  <> parse_remark_action

parse_give_action
  = command' "give" "make assignment submission" $ do
    assign <- argument str
              ( metavar "ASSIGN"
             <> help "the assignment to submit" )
    files  <- some (argument str
                    ( metavar "FILES..."
                   <> help "files to submit"))
    pure Give {..}

parse_check_action
  = command' "check" "show deadlines, submission status" $ do
    assign <- argument str
              ( metavar "ASSIGN"
             <> help "assignment to check" )
    pure Check {..}

parse_collect_action
  = command' "collect" "view copy of marked submission" $ do
    assign <- argument str
              ( metavar "ASSIGN"
             <> help "the assignment to collect" )
    pure Collect {..}

parse_assigns_action
  = command' "assigns" "show assignment names" $
    pure Assigns

parse_sturec_action
  = command' "sturec" "show class record" $
    pure Sturec

parse_mark_action
  = command' "mark" "assess submissions" $
    pure Mark

parse_rsms_action
  = command' "rsms" "update class records" $
    pure RSMS

parse_replace_action
  = command' "replace" "update a submission" $ do
    assign  <- argument str
               ( metavar "ASSIGN"
              <> help "the assignment ..." )
    group   <- argument str
               ( metavar "GROUP"
              <> help "the sub-group ..." )
    student <- argument str
               ( metavar "STUDENT"
              <> help "... and the student to replace" )
    tarfile <- argument str
               ( metavar "TAR-FILE"
              <> help "the submission tarball to replace" )
    backup  <- flag True False
               ( long "no-backup"
              <> help "don't preserve a backup" )
    pure Replace {..}

parse_remark_action
  = command' "remark" "rerun automated testing" $ do
    assign  <- argument str
               ( metavar "ASSIGN"
              <> help "the assignment ..." )
    group   <- argument str
               ( metavar "GROUP"
              <> help "the sub-group ..." )
    student <- argument str
               ( metavar "STUDENT"
              <> help "... and the student to remark" )
    pure Remark {..}

------------------------------------------------------------------------

getopt :: ParserInfo Action
getopt =
  info (parse_action <**> version_opt <**> helper)
    ( fullDesc
   <> progDesc descr
    )
  where
    descr = "Gateway to class account facilities."
    header_text =
      "classrun (adm-" ++ (showVersion version) ++ ") " ++
      "-- " ++ descr
    version_text = intercalate "\n" [ header_text ]
    version_opt =
      infoOption version_text
        ( long "version"
       <> help "Show version information."
       <> hidden
        )

command' :: String -> String -> Parser a -> Mod CommandFields a
command' name desc opts = command name (info opts (progDesc desc))

------------------------------------------------------------------------
